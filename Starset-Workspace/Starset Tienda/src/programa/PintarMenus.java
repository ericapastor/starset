package programa;

public class PintarMenus {

	/**
	 * Mostrar menu de inicio
	 */
	static void menuInicio() {
		System.out.println("|   *       _________________________________________           |");
		System.out.println("|                                                        *      |");
		System.out.println("|                                                               |");
		System.out.println("|     *                     *                                   |");
		System.out.println("|       *                                     *         *       |");
		System.out.println("|              *         MENU DE INICIO                         |");
		System.out.println("|                                                 *             |");
		System.out.println("|    *                *                        *                |");
		System.out.println("|                                                               |");
		System.out.println("|           _________________________________________           |");
		System.out.println("|                                                               |");
		System.out.println("|   *                                        *      *           |");
		System.out.println("|                                             *                 |");
		System.out.println("|           1 - Darse de alta                                   |");
		System.out.println("|                                                               |");
		System.out.println("|           2 - Iniciar sesion                                  |");
		System.out.println("|                                                               |");
		System.out.println("|    *                                                          |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Si quieres terminar el programa, pulsa 0            |");
		System.out.println("|   *                                                           |");
		System.out.println("|                                    *                          |");
	}

	/**
	 * Mostrar menu de administrador
	 */
	static void menuAdministrador() {
		System.out.println("|                                                               |");
		System.out.println("|     *                     *                                   |");
		System.out.println("|       *                                     *         *       |");
		System.out.println("|              *       MENU ADMINISTRADOR                       |");
		System.out.println("|                                                 *             |");
		System.out.println("|   *                 *                        *                |");
		System.out.println("|                                                               |");
		System.out.println("|           1 - Menu alta                                       |");
		System.out.println("|           2 - Menu buscar                                     |");
		System.out.println("|           3 - Menu listar                                     |");
		System.out.println("|           4 - Menu modificar                                  |");
		System.out.println("|           5 - Menu borrar                                     |");
		System.out.println("|           6 - Servicio semanal: tirada de cartas              |");
		System.out.println("|           7 - Guardar cambios                                 |");
		System.out.println("|                                                               |");
		System.out.println("|    *                                                          |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|   *                                                           |");
		System.out.println("|                                    *                          |");
	}

	/**
	 * Mostrar menu de cliente
	 */
	static void menuCliente() {
		System.out.println("|                                                               |");
		System.out.println("|     *                     *                                   |");
		System.out.println("|       *                                     *         *       |");
		System.out.println("|              *          MENU CLIENTE                          |");
		System.out.println("|                                                 *             |");
		System.out.println("|   *                 *                        *                |");
		System.out.println("|                                                               |");
		System.out.println("|           1 - Dar de alta productos                           |");
		System.out.println("|           2 - Buscar un producto                              |");
		System.out.println("|           3 - Listar productos                                |");
		System.out.println("|           4 - Modificar nombre de un producto                 |");
		System.out.println("|           5 - Borrar productos                                |");
		System.out.println("|           6 - Servicio semanal: tirada de cartas              |");
		System.out.println("|           7 - Gestionar datos introductidos                   |");
		System.out.println("|                                                               |");
		System.out.println("|    *                                                          |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|   *                                                           |");
		System.out.println("|                                    *                          |");
	}

	/**
	 * Mostrar menu alta productos
	 */
	static void menuAltaAdmin() {
		System.out.println("|                                                               |");
		System.out.println("|     *                     *                                   |");
		System.out.println("|       *                                     *         *       |");
		System.out.println("|              *            MENU ALTA                           |");
		System.out.println("|                                                 *             |");
		System.out.println("|   *                 *                        *                |");
		System.out.println("|                                                               |");
		System.out.println("|           1 - Dar de alta productos                           |");
		System.out.println("|           2 - Dar de alta clientes                            |");
		System.out.println("|                                                        *      |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|   *                                                           |");
		System.out.println("|                                    *                          |");
	}

	/**
	 * Mostrar menu alta productps
	 */
	static void menuAltaProd() {
		System.out.println("|                                                               |");
		System.out.println("|     *                     *                                   |");
		System.out.println("|       *                                     *         *       |");
		System.out.println("|              *       MENU ALTA PRODUCTOS                      |");
		System.out.println("|                                                 *             |");
		System.out.println("|   *                 *                        *                |");
		System.out.println("|                                                               |");
		System.out.println("|           1 - Dar de alta barajas                             |");
		System.out.println("|           2 - Dar de alta cristales                           |");
		System.out.println("|           3 - Dar de alta velas                               |");
		System.out.println("|                                                        *      |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|   *                                                           |");
		System.out.println("|                                    *                          |");
	}

	/**
	 * Mostrar menu buscar producto
	 */
	static void menuBuscarAdmin() {
		System.out.println("|                                                               |");
		System.out.println("|     *                     *                                   |");
		System.out.println("|       *                                     *         *       |");
		System.out.println("|              *           MENU BUSCAR                          |");
		System.out.println("|                                                 *             |");
		System.out.println("|   *                 *                        *                |");
		System.out.println("|                                                               |");
		System.out.println("|           1 - Buscar un producto                              |");
		System.out.println("|           2 - Buscar un cliente                               |");
		System.out.println("|                                                        *      |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|   *                                                           |");
		System.out.println("|                                    *                          |");
	}

	/**
	 * Menu buscar productos
	 */
	static void menuBuscarProd() {
		System.out.println("|                                                               |");
		System.out.println("|     *                     *                                   |");
		System.out.println("|       *                                     *         *       |");
		System.out.println("|              *      MENU BUSCAR PRODUCTOS                     |");
		System.out.println("|                                                 *             |");
		System.out.println("|   *                 *                        *                |");
		System.out.println("|                                                               |");
		System.out.println("|           1 - Buscar cartas por nombre                        |");
		System.out.println("|           2 - Buscar cristales por forma                      |");
		System.out.println("|           3 - Buscar velas por aroma                          |");
		System.out.println("|           4 - Buscar producto por codigo                      |");
		System.out.println("|                                                        *      |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|   *                                                           |");
		System.out.println("|                                    *                          |");
	}

	/**
	 * Menu listar del administrador
	 */
	static void menuListarAdmin() {
		System.out.println("|                                                               |");
		System.out.println("|     *                     *                                   |");
		System.out.println("|       *                                     *         *       |");
		System.out.println("|              *           MENU LISTAR                          |");
		System.out.println("|                                                 *             |");
		System.out.println("|   *                 *                        *                |");
		System.out.println("|                                                               |");
		System.out.println("|           1 - Listar productos                                |");
		System.out.println("|           2 - Listar usuarios                                 |");
		System.out.println("|                                                        *      |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|   *                                                           |");
		System.out.println("|                                    *                          |");
	}

	/**
	 * Menu listar productos
	 */
	static void menuListarProd() {
		System.out.println("|                                                               |");
		System.out.println("|     *                     *                                   |");
		System.out.println("|       *                                     *         *       |");
		System.out.println("|              *      MENU LISTAR PRODUCTOS                     |");
		System.out.println("|                                                 *             |");
		System.out.println("|   *                 *                        *                |");
		System.out.println("|                                                               |");
		System.out.println("|           1 - Listar barajas                                  |");
		System.out.println("|           2 - Listar cristales                                |");
		System.out.println("|           3 - Listar velas                                    |");
		System.out.println("|           4 - Listar todos los productos                      |");
		System.out.println("|                                                        *      |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|   *                                                           |");
		System.out.println("|                                    *                          |");
	}

	/**
	 * Mostrar menu modificar
	 */
	public static void menuModificar() {
		System.out.println("|                                                               |");
		System.out.println("|     *                     *                                   |");
		System.out.println("|       *                                     *         *       |");
		System.out.println("|              *         MENU MODIFICAR                         |");
		System.out.println("|                                                 *             |");
		System.out.println("|   *                 *                        *                |");
		System.out.println("|                                                               |");
		System.out.println("|           1 - Modificar productos                             |");
		System.out.println("|           2 - Modificar usuarios                      *       |");
		System.out.println("|                                                  *     *      |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|   *                                                           |");
	}

	/**
	 * Mostrar menu modificar
	 */
	public static void menuModificarProd() {
		System.out.println("|                                                               |");
		System.out.println("|     *                     *                                   |");
		System.out.println("|       *                                     *         *       |");
		System.out.println("|              *    MENU MODIFICAR PRODUCTOS                    |");
		System.out.println("|                                                 *             |");
		System.out.println("|   *                 *                        *                |");
		System.out.println("|                                                               |");
		System.out.println("|           1 - Modificar una baraja                            |");
		System.out.println("|           2 - Modificar cristales por forma           *       |");
		System.out.println("|      *    3 - Modificar velas por aroma          *            |");
		System.out.println("|    *      4 - Modificar un producto por codigo                |");
		System.out.println("|                                                  *     *      |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|   *                                                           |");
	}

	/**
	 * Escribir el menu de borrar
	 * 
	 * @see #pedirCodigo()
	 */
	public static void menuBorrar() {
		System.out.println("|                                                               |");
		System.out.println("|     *                     *                                   |");
		System.out.println("|       *                                     *         *       |");
		System.out.println("|              *           MENU BORRAR                          |");
		System.out.println("|                                                 *             |");
		System.out.println("|   *                 *                        *                |");
		System.out.println("|                                                               |");
		System.out.println("|           1 - Borrar cristales por forma                      |");
		System.out.println("|   *       2 - Borrar velas por aroma                  *       |");
		System.out.println("|  *        3 - Borrar producto por codigo            *         |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|                                    *                          |");
	}

}
