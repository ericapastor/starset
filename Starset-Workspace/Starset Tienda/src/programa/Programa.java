package programa;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

import clientes.User;
import enums.TipoUsuario;
import gestiones.Gestion;
import productos.Baraja;

/**
 * Clase Principal. El main tiene una sola linea que llama al metodo programa(),
 * el cual da de alta un objeto de gestion y unos productos base.
 * 
 * Despues va llamando a otros metodos estaticos de esta clase segun la opcion
 * que se elija en el menu.
 * 
 * @author Effy
 *
 */

public class Programa {

	/**
	 * Scanner unico para todo el programa. Es publico para que sea accesible desde
	 * otras clases.
	 */
	static public Scanner input = new Scanner(System.in);

	/**
	 * main
	 * 
	 * @param args String[]
	 */
	public static void main(String[] args) {
		Gestion gestion = new Gestion();
		gestion.cargarDatos();
		//gestion.altaUsersBase();
		programa(gestion);
	}

	/**
	 * Pide elegir una opcion, lo cual se repite siempre y cuando el usuario no
	 * inserte nada (pulse enter sin valores). Ante una opcion no contemplada se
	 * muestra un mensaje indicandolo. Ante introducir el 0 se termina el programa.
	 * En la opcion 1 se dan de alta clientes (nunca administradores) y en la opcion
	 * 2 se inicia sesion. Segun el tipo de usuario que inicie sesion, se redigira
	 * al menu cliente o al menu administrador. ########## (POR ESCRIBIR)
	 * ###########
	 * 
	 * @param gestion Gestion
	 * 
	 * @see Pintar#menuInicio()
	 * @see Pintar#despedida()
	 * @see Gestion#pedirDatosCliente()
	 * @see #pedirDatosString()
	 * 
	 */
	public static void programa(Gestion gestion) {
		Pintar.saludo();
		String opcion = "";
		while (!opcion.equalsIgnoreCase("0")) {
			PintarMenus.menuInicio();
			opcion = pedirDatosString();
			switch (opcion) {
			case "0":
				Pintar.despedida();
				input.close();
				System.exit(0);
			case "1":
				// solo se pueden dar de alta por aqui como clientes
				// nunca como administradores
				gestion.pedirDatosCliente();
				break;
			case "2":
				inicioSesion(gestion);
				// falta inicio de sesion aqui
				// acordarme de poner opcion de convertir a un cliente en administrador por otro
				// administrador, y de dar de alta administradores base
				break;
			}
		}
	}

	public static void inicioSesion(Gestion gestion) {
		User user = pedirUser(gestion);
		if (user != null) {
			if (user.getTipoUser() == TipoUsuario.ADMINISTRADOR) {
				menuAdministrador(gestion);
			} else {
				menuCliente(gestion, user);
			}
		}
	}

	public static User pedirUser(Gestion gestion) {
		Pintar.pedirNickname();
		boolean userExiste = false;
		while (!userExiste) {
			String nick = pedirDatosString();
			if (nick.equals("0")) {
				Pintar.retrocediendo();
				break;
			} else {
				for (User u : gestion.getUsers()) {
					if (u.getNickname().equals(nick)) {
						userExiste = true;
						if (pedirPasswd(u)) {
							return u;
						} else {
							Pintar.pwdIncorrecta();
						}
					}
				}
				if (!userExiste) {
					Pintar.nickNoExiste();
				}
			}
		}
		return null;
	}

	public static boolean pedirPasswd(User u) {
		Pintar.pedirPasswd();
		String pwd = pedirDatosString();
		if (pwd.equals(u.getPasswd())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param gestion Gestion
	 */
	public static void menuCliente(Gestion gestion, User user) {
		// por escribir
	}

	/**
	 * Pide elegir una opcion, lo cual se repite siempre y cuando el usuario no
	 * inserte nada (pulse enter sin valores). Ante una opcion no contemplada se
	 * muestra un mensaje indicandolo. Ante introducir el 0 se vuelve hacia atras.
	 * Del 1 al 6 se reconduce a otros metodos de esta clase.
	 * 
	 * @param gestion Gestion
	 * 
	 * @see Pintar#saludo()
	 * @see Pintar#menuAdministrador()
	 * @see Pintar#opcionNoValida()
	 * @see #pedirDatosString()
	 * @see #opcion1(Gestion)
	 * @see #opcion2(Gestion)
	 * @see #opcion3(Gestion)
	 * @see #opcion4(Gestion)
	 * @see #opcion5(Gestion)
	 * @see #opcion6(Gestion, int)
	 * @see #opcion7(Gestion)
	 * 
	 */
	private static void menuAdministrador(Gestion gestion) {
		int contadorLectura = 0;
		String opcion = "";
		while (!opcion.equalsIgnoreCase("0")) {
			PintarMenus.menuAdministrador();
			opcion = pedirDatosString();
			switch (opcion) {
			case "0":
				Pintar.retrocediendo();
				break;
			case "1":
				opcion1(gestion);
				break;
			case "2":
				opcion2(gestion);
				break;
			case "3":
				opcion3(gestion);
				break;
			case "4":
				opcion4(gestion);
				break;
			case "5":
				opcion5(gestion);
				break;
			case "6":
				contadorLectura = opcion6(gestion, contadorLectura);
				break;
			case "7":
				opcion7(gestion);
				break;
			default:
				Pintar.opcionNoValida();
				break;
			}
		}
	}

	/**
	 * Se pide elegir una opcion, lo cual se repite siempre y cuando el usuario no
	 * inserte nada (pulse enter sin valores). Ante una opcion no contemplada se
	 * muestra un mensaje indicandolo. Ante introducir el 0 se vuelve al menu
	 * principal. Del 1 al 2 se reconduce a metodos de Gestion.
	 * 
	 * @param gestion Gestion
	 * 
	 * @see Pintar#menuAlta()
	 * @see Pintar#retrocediendo()
	 * @see Pintar#opcionNoValida()
	 * @see Gestion#pedirDatosBaraja()
	 * @see Gestion#pedirDatosVelas()
	 * @see Gestion#pedirDatosCristal()
	 * @see #pedirDatosString()
	 * 
	 */
	private static void opcion1(Gestion gestion) {
		String opcion = "";
		while (!opcion.equalsIgnoreCase("0")) {
			PintarMenus.menuAltaAdmin();
			opcion = pedirDatosString();
			switch (opcion) {
			case "0":
				Pintar.retrocediendo();
				break;
			case "1":
				gestion.pedirDatosBaraja();
				break;
			case "2":
				gestion.pedirDatosCristal();
				break;
			case "3":
				gestion.pedirDatosVelas();
				break;
			case "4":
				// por escribir
				break;
			default:
				Pintar.opcionNoValida();
				break;
			}
		}
	}

	/**
	 * Se pide elegir una opcion, lo cual se repite siempre y cuando el usuario no
	 * inserte nada (pulse enter sin valores). Ante una opcion no contemplada se
	 * muestra un mensaje indicandolo. Ante introducir el 0 se vuelve al menu
	 * principal. Del 1 al 4 se reconduce a metodos de Gestion.
	 * 
	 * @param gestion Gestion
	 * 
	 * @see Pintar#menuBuscar()
	 * @see Pintar#retrocediendo()
	 * @see Pintar#opcionNoValida()
	 * @see Gestion#buscarCarta()
	 * @see Gestion#buscarCristalPorForma()
	 * @see Gestion#buscarVelaPorAroma()
	 * @see Gestion#buscarProducto()
	 * @see #pedirDatosString()
	 * 
	 */
	private static void opcion2(Gestion gestion) {
		String opcion = "";
		while (!opcion.equalsIgnoreCase("0")) {
			PintarMenus.menuBuscarAdmin();
			opcion = pedirDatosString();
			switch (opcion) {
			case "0":
				Pintar.retrocediendo();
				break;
			case "1":
				gestion.buscarCarta();
				break;
			case "2":
				gestion.buscarCristalPorForma();
				break;
			case "3":
				gestion.buscarVelaPorAroma();
				break;
			case "4":
				gestion.buscarProducto();
				break;
			default:
				Pintar.opcionNoValida();
				break;
			}
		}
	}

	/**
	 * Se pide elegir una opcion, lo cual se repite siempre y cuando el usuario no
	 * inserte nada (pulse enter sin valores). Ante una opcion no contemplada se
	 * muestra un mensaje indicandolo. Ante introducir el 0 se vuelve al menu
	 * principal. Del 1 al 3 se reconduce a metodos de Gestion.
	 * 
	 * @param gestion Gestion
	 * 
	 * @see Pintar#menuListar()
	 * @see Pintar#retrocediendo()
	 * @see Pintar#opcionNoValida()
	 * @see Gestion#listarProductos(int)
	 * @see Gestion#listarProductos()
	 * 
	 */
	private static void opcion3(Gestion gestion) {
		String opcion = "";
		while (!opcion.equalsIgnoreCase("0")) {
			PintarMenus.menuListarAdmin();
			opcion = pedirDatosString();
			switch (opcion) {
			case "0":
				Pintar.retrocediendo();
				break;
			case "1":
				gestion.listarProductos(1);
				break;
			case "2":
				gestion.listarProductos(2);
				break;
			case "3":
				gestion.listarProductos(3);
				break;
			case "4":
				gestion.listarProductos();
				break;
			default:
				Pintar.opcionNoValida();
				break;
			}
		}
	}

	/**
	 * Se pide el codigo del producto a modificar.
	 * 
	 * @param gestion Gestion
	 * 
	 * @see Gestion#modificarProductoPorCodigo()
	 * 
	 */
	private static void opcion4(Gestion gestion) {
		gestion.modificarProductoPorCodigo();
	}

	/**
	 * Se pide el codigo del producto a borrar.
	 * 
	 * @param gestion Gestion
	 * 
	 * @see Gestion#borrarProductoPorCodigo()
	 * 
	 */
	private static void opcion5(Gestion gestion) {
		gestion.borrarProductoPorCodigo();
	}

	/**
	 * Recibe un contador del menu principal y devuelve un numero. Si el contador
	 * que recibe es superior a 0, no permite acceder a la lectura.
	 * 
	 * Si el contador es 0, redirige al metodo de lectura de la clase baraja.
	 * 
	 * Anteriormente tenia aqui un menu para elegir la baraja con la que quieres la
	 * tirada, pero no me dio tiempo a rellenar una segunda baraja asi que lo deje
	 * sin opciones, redirigiendote a la unica de la que las cartas estan rellenas.
	 * 
	 * @param gestion Gestion
	 * @param c       int
	 * 
	 * @see Pintar#noMasLecturas()
	 * @see Pintar#menuTirada()
	 * @see Gestion#getProductos()
	 * @see Baraja#lecturaTresCartas()
	 * 
	 */
	private static int opcion6(Gestion gestion, int c) {
		if (c > 0) {
			Pintar.noMasLecturas();
		} else {
			Pintar.menuTirada();
			Baraja b = (Baraja) gestion.getProductos().get(0);
			b.lecturaTresCartas();

		}
		return 1;
	}

	/**
	 * Se pide elegir una opcion, lo cual se repite siempre y cuando el usuario no
	 * inserte nada (pulse enter sin valores). Ante una opcion no contemplada se
	 * muestra un mensaje indicandolo. Ante introducir el 0 se vuelve al menu
	 * principal. Del 1 al 2 se reconduce a metodos de Gestion.
	 * 
	 * @param gestion
	 * 
	 * @see Pintar#menuGestionProd()
	 * @see Pintar#retrocediendo()
	 * @see Pintar#opcionNoValida()
	 * @see Gestion#guardarDatosProductos()
	 * @see Gestion#cargarDatosProductos()
	 * @see #pedirDatosString()
	 * 
	 */

	private static void opcion7(Gestion gestion) {
		gestion.guardarDatosAdmin();
	}

	/**
	 * Pedir un dato String, comprobando que se escriba algo. Devuelve lo que sea
	 * que se escriba.
	 * 
	 * @return datos String
	 * 
	 * @see Pintar#noHasEscritoNada()
	 * 
	 */
	public static String pedirDatosString() {
		String datos = "";
		while (datos.equals("")) {
			datos = input.nextLine();
			if (!datos.equals("")) {
				break;
			} else {
				Pintar.noHasEscritoNada();
			}
		}
		return datos;
	}

	/**
	 * Pedir un dato int, con comprobacion de excepciones.
	 * 
	 * @return datos int
	 * 
	 * @see Pintar#noNegativos()
	 * @see Pintar#valorIncorrecto()
	 * @see Pintar#noDecimales()
	 * 
	 */
	public static int pedirDatosInt() {
		int datos = -1;
		while (datos < 0) {
			try {
				datos = input.nextInt();
				input.nextLine();
				if (datos < 0) {
					Pintar.noNegativos();
				} else {
					return datos;
				}
			} catch (Exception InputMismatchException) {
				Pintar.valorIncorrecto();
				Pintar.noDecimales();
				input.nextLine();
			}
		}
		return datos;
	}

	/**
	 * Pedir un dato float, con comprobacion de excepciones.
	 * 
	 * @return datos float
	 * 
	 * @see Pintar#noNegativos()
	 * @see Pintar#valorIncorrecto()
	 * @see Pintar#decimales()
	 * 
	 */
	public static float pedirDatosFloat() {
		float datos = -1F;
		while (datos < 0) {
			try {
				datos = input.nextFloat();
				input.nextLine();
				if (datos < 0) {
					Pintar.noNegativos();
				} else {
					return datos;
				}
			} catch (Exception InputMismatchException) {
				Pintar.valorIncorrecto();
				Pintar.decimales();
				input.nextLine();
			}
		}
		return datos;
	}

	/**
	 * Pide una cadena de texto hasta que se introduzca si o no, devolviendo true o
	 * false respectivamente.
	 * 
	 * @return bool boolean
	 * 
	 * @see #pedirDatosString()
	 * @see Pintar#noHasEscritoNada()
	 * @see Pintar#booleanSINO()
	 * 
	 */
	public static boolean pedirDatosBoolean() {
		String bool = "";
		while (!bool.equalsIgnoreCase("si") && !bool.equalsIgnoreCase("no")) {
			bool = pedirDatosString();
			bool = bool.toUpperCase();
			switch (bool) {
			case "SI":
				return true;
			case "NO":
				return false;
			case "":
				Pintar.noHasEscritoNada();
			default:
				Pintar.booleanSINO();
			}
		}
		return false;
	}

	public static String pedirPasswd() {
		String p = "";
		while (!p.equals("0")) {
			p = pedirDatosString();
			int c1 = 0;
			int c2 = 0;
			int c3 = 0;
			int c4 = 0;
			for (int i = 0; i < p.length(); i++) {
				if ((p.charAt(i) >= 33 && p.charAt(i) <= 47) || (p.charAt(i) >= 58 && p.charAt(i) <= 64)
						|| (p.charAt(i) >= 91 && p.charAt(i) <= 60) || (p.charAt(i) >= 123 && p.charAt(i) <= 126)) {
					c1++;
				} else if (p.charAt(i) >= 48 && p.charAt(i) <= 57) {
					c2++;
				} else if (p.charAt(i) >= 65 && p.charAt(i) <= 90) {
					c3++;
				} else if (p.charAt(i) >= 97 && p.charAt(i) <= 122) {
					c4++;
				}
			}
			if (c1 > 0 && c2 > 0 && c3 > 0 && c4 > 0) {
				return p;
			} else {
				System.out.println("|                                    *                          |");
				System.out.println("|           La password tiene que tener al menos                |");
				System.out.println("|   *       un caracter especial, un numero, una        *       |");
				System.out.println("|  *        mayuscula, y una minuscula, vuelve a      *         |");
				System.out.println("|           intentarlo                                          |");
				System.out.println("|                                                               |");
			}
		}
		return p;
	}

	public static LocalDate pedirDatosFecha() {
		while (true) {
			String f = pedirDatosString();
			try {
				LocalDate fecha = LocalDate.parse(f);
				return fecha;
			} catch (DateTimeParseException e) {
				Pintar.formatoFecha();
			}
		}
	}

	public static LocalTime pedirDatosHora() {
		while (true) {
			String f = pedirDatosString();
			try {
				LocalTime hora = LocalTime.parse(f);
				return hora;
			} catch (DateTimeParseException e) {
				Pintar.formatoHora();
			}
		}
	}

	/**
	 * Recibe un frase, capitaliza la primera letra, y pone en minusculas las
	 * siguientes, devolviendo asi una frase.
	 * 
	 * @param s String
	 * @return s String
	 */
	public static String toCamelCase(String s) {
		return s.toUpperCase().charAt(0) + s.toLowerCase().substring(1);
	}

}
