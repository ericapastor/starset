package programa;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * La clase Pintar es estatica y me sirve dejar el codigo mas limpio
 * 
 * @author Effy
 *
 */

public class Pintar {

	/**
	 * Pintar inicio
	 */
	static void saludo() {
		System.out.println("|                                    *                       *  |");
		System.out.println("|        *                                  *             *     |");
		System.out.println("|                 *                                *            |");
		System.out.println("|           *                                         *         |");
		System.out.println("|                    *                                       *  |");
		System.out.println("|  *       *                                       *            |");
		System.out.println("|                            #  #  #                            |");
		System.out.println("|                        #  # #  #     #                *  *    |");
		System.out.println("|    *                #  #  #  #         #                      |");
		System.out.println("|                   #  #  #  #             #                    |");
		System.out.println("|                  #  #  #  #               #                   |");
		System.out.println("|                 #  #  #  #   Bienvenido    #            *     |");
		System.out.println("|                 #  #  #  #   a starset*    #                  |");
		System.out.println("| *               #  #  #  #                 #           *      |");
		System.out.println("|                  #  #  #  #               #               *   |");
		System.out.println("|                   #  #  #  #             #       *            |");
		System.out.println("|                     #  #  #  #         #                      |");
		System.out.println("|        *               #  # #  #     #             *          |");
		System.out.println("|     *                      #  #  #           *             *  |");
		System.out.println("|                                                               |");
		System.out.println("|     *      *                                    *             |");
		System.out.println("|         *        *                            *      *    **  |");
		System.out.println("|                               *        **                *    |");
		System.out.println("|       *                                       *             * |");
		System.out.println("|                                                               |");
		System.out.println("|                                                               |");
	}

	/**
	 * Pintar despedida
	 */
	public static void despedida() {
		System.out.println("|                                                               |");
		System.out.println("|                                                               |");
		System.out.println("|                                                               |");
		System.out.println("|                                                               |");
		System.out.println("|                                                               |");
		System.out.println("|                                    *                       *  |");
		System.out.println("|        *                                  *             *     |");
		System.out.println("|                 *                                *            |");
		System.out.println("|           *                                         *         |");
		System.out.println("|                    *                                       *  |");
		System.out.println("|  *       *                                       *            |");
		System.out.println("|                            #  #  #                            |");
		System.out.println("|                        #  # #  #     #                *  *    |");
		System.out.println("|    *                #  #  #  #         #                      |");
		System.out.println("|                   #  #  #  #             #                    |");
		System.out.println("|                  #  #  #  #               #                   |");
		System.out.println("|                 #  #  #  #   Gracias,      #            *     |");
		System.out.println("|                 #  #  #  #   hasta         #                  |");
		System.out.println("| *               #  #  #  #   pronto        #           *      |");
		System.out.println("|                  #  #  #  #               #               *   |");
		System.out.println("|                   #  #  #  #             #       *            |");
		System.out.println("|                     #  #  #  #         #                      |");
		System.out.println("|        *               #  # #  #     #             *          |");
		System.out.println("|     *                      #  #  #           *             *  |");
		System.out.println("|                                                               |");
		System.out.println("|     *      *                                    *             |");
		System.out.println("|         *        *                            *      *    **  |");
		System.out.println("|                               *        **                *    |");
		System.out.println("|       *                                       *             * |");
	}

	/**
	 * Mostrar mensaje de alta de productos
	 */
	public static void datosAltas() {
		System.out.println("|                                                        *      |");
		System.out.println("|                                                               |");
		System.out.println("|           Bienvenido al proceso de alta.                      |");
		System.out.println("|                                                               |");
		System.out.println("|           Si quieres volver hacia atras en algun              |");
		System.out.println("|   *       momento, pulsa 0.                                   |");
		System.out.println("|                                    *                          |");
	}

	/**
	 * Mostrar mensaje para buscar producto por codigo
	 * 
	 * @see #pedirCodigo()
	 */
	public static void buscarPorCodigo() {
		System.out.println("|      *                                           *            |");
		System.out.println("|   *       Escribe el codigo del producto              *       |");
		System.out.println("|           que quieres buscar                                  |");
		System.out.println("|  *                                                  *         |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|                                    *                          |");
		Pintar.pedirCodigo();
	}

	/**
	 * Escribir el menu de servicio tirada tres cartas
	 */
	static void menuTirada() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Esta es la baraja disponible para           *       |");
		System.out.println("| *         la tirada: Mazo de los Espiritus               *    |");
		System.out.println("|    *      Animales Salvajes Desconocidos.           *         |");
		System.out.println("|                                                        *      |");
		System.out.println("|        *                                                      |");
		System.out.println("|           Esta es la lectura que ha salido:                   |");
		System.out.println("|                                                        *      |");
	}

	/**
	 * Mostrar mensaje de opcion no contemplada
	 */
	static void opcionNoValida() {
		System.out.println("|                                              *                |");
		System.out.println("|           Opcion no contemplada                               |");
		System.out.println("|          *                                                    |");
	}

	/**
	 * Mostrar mensaje de retrocediendo
	 */
	public static void retrocediendo() {
		System.out.println("|                                                 *             |");
		System.out.println("|           Retrocediendo...                         *          |");
		System.out.println("|      *                                       *                |");
	}

	/**
	 * Mostrar mensaje de "no has escrito nada"
	 */
	static void noHasEscritoNada() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           No has escrito nada                         *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para indicar que un valor es incorrecto
	 */
	public static void valorIncorrecto() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Has introducido un valor incorrecto         *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje que pide no introducir decimales
	 */
	static void noDecimales() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Recuerda no introducir decimales            *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje que pide no introducir numeros negativos
	 */
	static void noNegativos() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           No puedes introducir numeros negativos      *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para indicar que un valor introducido deberia ser numerico
	 */
	static void decimales() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Recuerda separar los decimales con          *       |");
		System.out.println("|           una coma, nunca con un punto                        |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para indicar que se introduzca solamente SI o NO
	 */

	static void booleanSINO() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Por favor escribe solamente SI o NO         *       |");
		System.out.println("|  *                                                  *         |");
	}

	public static void intentaloDeNuevo() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Intentemoslo de nuevo:                      *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para informar de la no alta de un producto
	 */
	public static void altaCancelada() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           El alta ha quedado cancelada                *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Preguntar si se quiere repetir otro alta de cliente
	 */
	public static void repetirAltaCliente() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Quieres introducir otro cliente [SI/NO]     *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Preguntar si se quiere repetir otro alta de carta
	 */
	public static void repetirAltaCarta() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Quieres introducir otra carta [SI/NO]       *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Preguntar si se quiere repetir otro alta de baraja
	 */
	public static void repetirAltaBaraja() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Quieres introducir otra baraja [SI/NO]      *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Preguntar si se quiere repetir otro alta de vela
	 */
	public static void repetirAltaVela() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Quieres introducir otra vela [SI/NO]        *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Preguntar si se quiere repetir otro alta de cristal
	 */
	public static void repetirAltaCristal() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Quieres introducir otro cristal [SI/NO]     *       |");
		System.out.println("|  *                                                  *         |");
	}

	public static void confirmacionCliente(String nombre, String pwd, LocalDate f, LocalTime h) {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Estos son los datos introducidos:           *       |");
		System.out.println("|                                                               |");
		System.out.println("|           * Nombre: " + nombre);
		System.out.println("|           * Password: " + pwd);
		System.out.println("|           * Fecha de nacimiento: " + f);
		System.out.println("|           * Hora de nacimiento: " + h);
		System.out.println("|  *                                                  *         |");
		System.out.println("|           Confirme si son correctos: [SI/NO]                  |");
		System.out.println("|                                                               |");
	}

	/**
	 * Mostrar datos instrucidos de una carta
	 * 
	 * @param nombre String
	 * @param peso   float
	 * @param precio float
	 * @param n      int
	 * @param tipo   String
	 */
	public static void confirmacionCarta(String nombre, String elemento, String int1, String int2, String consejo) {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Estos son los datos introducidos:           *       |");
		System.out.println("|                                                               |");
		System.out.println("|           * Nombre: " + nombre);
		System.out.println("|           * Elemento: " + elemento);
		System.out.println("|           * Interpretacion 1: " + int1);
		System.out.println("|           * Interpretacion 2: " + int2);
		System.out.println("|           * Consejo: " + consejo);
		System.out.println("|  *                                                  *         |");
		System.out.println("|           Confirme si son correctos: [SI/NO]                  |");
		System.out.println("|                                                               |");
	}

	/**
	 * Mostrar datos instrucidos de una baraja
	 * 
	 * @param nombre String
	 * @param peso   float
	 * @param precio float
	 * @param n      int
	 * @param tipo   String
	 */
	public static void confirmacionBaraja(String nombre, float peso, float precio, int n, String tipo) {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Estos son los datos introducidos:           *       |");
		System.out.println("|                                                               |");
		System.out.println("|           * Nombre: " + nombre);
		System.out.println("|           * Precio bruto: " + precio + " euros");
		System.out.println("|           * Peso: " + peso + "g");
		System.out.println("|           * Numero de cartas: " + n);
		System.out.println("|           * Tipo de baraja: " + tipo);
		System.out.println("|  *                                                  *         |");
		System.out.println("|           Confirme si son correctos: [SI/NO]                  |");
		System.out.println("|                                                               |");
	}

	/**
	 * Mostrar datos instrucidos de vela
	 * 
	 * @param aroma    String
	 * @param unidades int
	 * @param precio   float
	 * @param peso     float
	 * @param diametro float
	 * @param altura   float
	 */
	public static void confirmacionVela(String aroma, int unidades, float precio, float peso, float diametro,
			float altura) {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Estos son los datos introducidos:           *       |");
		System.out.println("|                                                               |");
		System.out.println("|           * Aroma: " + aroma);
		System.out.println("|           * Unidades: " + unidades);
		System.out.println("|           * Precio bruto: " + precio + " euros");
		System.out.println("|           * Peso: " + peso + "g");
		System.out.println("|           * Diametro: " + diametro + "cm");
		System.out.println("|           * Altura: " + altura + "cm");
		System.out.println("|  *                                                  *         |");
		System.out.println("|           Confirme si son correctos: [SI/NO]                  |");
		System.out.println("|                                                               |");
	}

	/**
	 * Mostrar datos instrucidos de cristal
	 *
	 * @param nombre   String
	 * @param peso     float
	 * @param precio   float
	 * @param isPulido boolean
	 * @param forma    String
	 */
	public static void confirmacionCristal(String nombre, float peso, float precio, boolean isPulido, String forma) {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Estos son los datos introducidos:           *       |");
		System.out.println("|                                                               |");
		System.out.println("|           * Nombre: " + nombre);
		System.out.println("|           * Peso: " + peso);
		System.out.println("|           * Precio bruto: " + precio + " euros");
		System.out.println("|           * Esta pulido: " + (isPulido ? "Si" : "No"));
		System.out.println("|           * Forma: " + forma);
		System.out.println("|  *                                                  *         |");
		System.out.println("|           Confirme si son correctos: [SI/NO]                  |");
		System.out.println("|                                                               |");
	}

	/**
	 * Mostrar mensaje de fin de proceso de altas
	 */
	public static void altasFinalizadas() {
		System.out.println("|                                                        *      |");
		System.out.println("|                                                               |");
		System.out.println("|           El proceso de alta ha terminado. Se te              |");
		System.out.println("|           redirigira al menu principal.                   *   |");
		System.out.println("|    *                                                          |");
		System.out.println("|        *                                                      |");
	}

	/**
	 * Mensaje para
	 */
	public static void cambiarCarta() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Que carta quieres cambiar:                  *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Ir mostrando por que carta vamos al ir insertando nuevas
	 */
	public static void altaCartaNum(int n) {
		System.out.println("|      *                                                        |");
		System.out.println("|           Vamos con la carta numero " + (n + 1) + "                  *      |");
		System.out.println("|                                                               |");
	}

	/**
	 * Ir mostrando por que producto vamos al ir insertando nuevos
	 *
	 * @param n int
	 */
	public static void altaProdNum(int n) {
		System.out.println("|      *                                                        |");
		System.out.println("|           Vamos con el producto numero " + (n + 1) + "               *      |");
		System.out.println("|                                                               |");
	}

	/**
	 * Mostrar un mensaje antes de realizar un pedido
	 */
	public static void altaPedido() {
		System.out.println("|      *                                                        |");
		System.out.println("|           Vamos con tu pedido                         *       |");
		System.out.println("|                                                               |");
	}

	/**
	 * Mensaje pedir nombre de un producto
	 */
	public static void pedirNombreProducto() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el nombre del producto            *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir nombre de un cliente
	 */
	public static void pedirNombreCliente() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el nombre completo del cliente    *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir el nickname
	 */
	public static void pedirNickname() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce un nombre de usuario              *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir el nickname
	 */
	public static void nickRepetido() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Ese nombre de usuario ya existe             *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Decir que el nickname introducido no existe
	 */
	public static void nickNoExiste() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Ese nombre de usuario no existe             *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir la password
	 */
	public static void pedirPasswd() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce una password                      *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para decir que la password es incorrecta
	 */
	public static void pwdIncorrecta() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Passwd incorrecta                           *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir fecha de nacimiento de un cliente
	 */
	public static void pedirFechaNacimiento() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce la fecha de nacimiento            *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir hora de nacimiento de un cliente
	 */
	public static void pedirHoraNacimiento() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce la hora de nacimiento             *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir peso de un producto
	 */
	public static void pedirPeso() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el peso del producto              *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir precio bruto
	 */
	public static void pedirPrecio() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el precio bruto del producto      *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir elemento de una carta
	 */
	public static void pedirElemento() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el elemento de la carta           *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir interpretacion de una carta
	 */
	public static void pedirInterpretacion(int n) {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce la interpretacion de la carta     *       |");
		System.out.println("|           cuando esta "
				+ (n == 0 ? "del derecho                             |" : "del reves                               |"));
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir consejo de una carta
	 */
	public static void pedirConsejo() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el consejo de la carta            *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir numero de cartas
	 */
	public static void pedirNumCartas() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce cuantas cartas tiene la baraja    *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir tipo de baraja
	 */
	public static void pedirTipoBaraja() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el tipo de baraja                 *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir aroma de la vela
	 */
	public static void pedirAromaVela() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el aroma de la vela               *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje que muestra las opciones de tipos de producto
	 */
	public static void opcionesTiposP() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Los tipos de productos que hay son:         *       |");
		System.out.println("|           * Baraja                                            |");
		System.out.println("|           * Vela                                              |");
		System.out.println("|           * Cristal                                           |");
	}

	/**
	 * Mensaje que muestra las opciones de tipos de barajas
	 */
	public static void opcionesTiposB() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Los tipos de barajas que hay son:           *       |");
		System.out.println("|           * Tarot                                             |");
		System.out.println("|           * Oraculo                                           |");
	}

	/**
	 * Mensaje que muestra las opciones de aromas
	 */
	public static void opcionesAromas() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Las opciones de aromas que hay son:         *       |");
		System.out.println("|           * Vainilla                                          |");
		System.out.println("|           * Limon                                             |");
		System.out.println("|           * Frutos rojos                                      |");
		System.out.println("|           * Canela                                            |");
		System.out.println("|           * Ninguno                                           |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir unidades
	 */
	public static void pedirUnidadesVela() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce cuantas unidades vienen           *       |");
		System.out.println("|  *        en el paquete de velas                    *         |");
		System.out.println("|                                       *                       |");
	}

	/**
	 * Mensaje pedir diametro de una vela
	 */
	public static void pedirDiametro() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el diametro de la vela            *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir altura de una vela
	 */
	public static void pedirAltura() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce la altura de la vela              *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje preguntar si un cristal esta pulido o no
	 */
	public static void pedirPulido() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Di si el cristal esta pulido: [SI/NO]       *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje pedir forma del cristal
	 */
	public static void pedirForma() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce la forma del cristal              *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para pedir nombre de una carta
	 */
	public static void pedirNombreCarta() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el nombre de la carta             *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para pedir el codigo de un producto en el menu de busqueda por codigo
	 */
	public static void pedirCodigo() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Escribe el codigo del producto              *       |");
		System.out.println("|   *                                                           |");
		System.out.println("|           Recuerda que tiene seis caracteres:                 |");
		System.out.println("|           Las tres primeras letras del tipo                   |");
		System.out.println("|           de producto, y tres numeros                         |");
		System.out.println("|      *                                                 *      |");
		System.out.println("|           Si quieres volver hacia atras, pulsa 0              |");
		System.out.println("|   *                                                           |");
	}

	/**
	 * Mensaje para pedir el nuevo nombre de un producto en el menu modificar
	 */
	public static void nuevoNombre() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Introduce el nuevo nombre que quie-         *       |");
		System.out.println("|           res darle al producto:                              |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para comunicar que el nombre ha sido modificado
	 */
	public static void nombreModificado() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           El nombre ha sido modificado                *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mensaje para informar de que las barajas no se pueden borrar
	 */
	public static void noSePuedenBorrarBarajas() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           No se pueden borrar las barajas             *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Escribir que un numero de carta no existe
	 */
	public static void cartaNoExiste() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           La carta no existe                          *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Escribir que la baraja no ha sido encontrada
	 */
	public static void barajaNoExiste() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           No se ha encontrado la baraja               *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Escribir que no se ha encontrado ningun producto con el codigo introducido
	 */
	public static void productoNoExiste() {
		System.out.println("|      *                                                        |");
		System.out.println("|           No se ha encontrado ningun producto    *            |");
		System.out.println("|           con ese codigo                              *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Escribir que no se encontraron coincidencias
	 */
	public static void noHayCoincidencias() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           No se han encontrado coincidencias          *       |");
		System.out.println("|  *                                                  *         |");
	}

	public static void esoEsTodo() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Esas son todas las coincidencias            *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Escribir que se ha llevado a cabo el borrado correctamente
	 */
	public static void borradoCorrecto() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Se ha/n eliminado el/los productos          *       |");
		System.out.println("|  *                                                  *         |");

	}

	/**
	 * Mostrar mensaje para luego mostrar la primera carta en una lectura
	 */
	public static void cartaPasado() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Donde solias estar:                         *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mostrar mensaje para luego mostrar la segunda carta en una lectura
	 */
	public static void cartaPresente() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Donde estas ahora:                          *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mostrar mensaje para luego mostrar la tercera carta en una lectura
	 */
	public static void cartaFuturo() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Donde estaras pronto:                       *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mostrar mensaje indicando que solamente se admite una lectura al dia
	 */
	public static void noMasLecturas() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Solo puedes acceder a una lectura           *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mostrar mensaje indicando que algo ha salido mal en la lectura
	 */
	public static void algoFueMal() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Algo ha salido mal en la lectura            *       |");
		System.out.println("|  *                                                  *         |");
	}

	/**
	 * Mostrar mensaje indicando que no hay cartas en la baraja
	 */
	public static void noHayCartas() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           No hay cartas en esta baraja todavia        *       |");
		System.out.println("|  *                                                  *         |");
	}

	public static void error() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Vaya, parece que ha habido un error...       *      |");
		System.out.println("|  *                                                  *         |");
	}

	public static void guardarDatos() {
		System.out.println("|      *                                           *            |");
		System.out.println("|           Guardando datos...                           *      |");
		System.out.println("|  *                                                  *         |");
	}

	public static void cargarDatos() {
		System.out.println("|                                                               |");
		System.out.println("|                                                               |");
		System.out.println("|      *                                           *            |");
		System.out.println("|           Cargando datos desde fichero...              *      |");
		System.out.println("|  *                                                  *         |");
	}

	public static void errorEntradaSalida() {
		System.out.println("|  *                                                  *         |");
		System.out.println("|           Error de entrada/salida                             |");
		System.out.println("|                                          *                    |");
	}

	public static void errorClaseNoEncontrada() {
		System.out.println("|  *                                                  *         |");
		System.out.println("|           Error, clase no encontrada        *                 |");
		System.out.println("|                                          *                    |");
	}

	public static void errorArchivoNoEncontrado() {
		System.out.println("|  *                                                  *         |");
		System.out.println("|           Error, archivo no encontrado      *                 |");
		System.out.println("|                                          *                    |");
	}

	public static void errorOrdenarPorCodigo() {
		System.out.println("|  *                                                  *         |");
		System.out.println("|           Error, no se pudo ordenar los productos             |");
		System.out.println("|                                          *                    |");
	}

	public static void formatoFecha() {
		System.out.println("|  *                                                            |");
		System.out.println("| *         El formato de la fecha debe ser:         *          |");
		System.out.println("|                                                           *   |");
		System.out.println("|          *        ' YYYY-MM-DD '                      *       |");
		System.out.println("|                                                               |");
		System.out.println("|           De 1947 a 2010 anhyos, de 1 a 12 meses              |");
		System.out.println("|           y de 1 a 28, 30 o 31 dias segun el mes              |");
	}

	public static void formatoHora() {
		System.out.println("|  *                                                            |");
		System.out.println("| *         El formato de la hora debe ser:          *          |");
		System.out.println("|                                                           *   |");
		System.out.println("|          *           ' HH-mm '                        *       |");
		System.out.println("|                                                               |");
		System.out.println("|           De 0 a 23 horas y de 0 a 59 minutos                 |");
		System.out.println("|                                                               |");
	}
}
