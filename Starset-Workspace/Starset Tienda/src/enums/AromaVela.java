package enums;

/**
 * @author erica
 *
 */

public enum AromaVela {
	NINGUNO, VAINILLA, LIMON, FRUTOS_ROJOS, CANELA;

	public String aroma() {
		switch (this) {
		case VAINILLA:
			return "Vainilla";
		case LIMON:
			return "Limon";
		case FRUTOS_ROJOS:
			return "Futos rojos";
		case CANELA:
			return "Canela";
		default:
			return "Ninguno";
		}
	}

	public String nombre() {
		switch (this) {
		case VAINILLA:
			return "Caricias de Algodon";
		case LIMON:
			return "Frescura Vespertina";
		case FRUTOS_ROJOS:
			return "Pasion Encendida";
		case CANELA:
			return "Actividad Matutina";
		default:
			return "Vela Estandar para Rituales";
		}
	}

	public String color() {
		switch (this) {
		case VAINILLA:
			return "#FFFCF0";
		case LIMON:
			return "#FDFF90";
		case FRUTOS_ROJOS:
			return "#B41E2B";
		case CANELA:
			return "#CB9943";
		default:
			return "#000000";
		}
	}
}
