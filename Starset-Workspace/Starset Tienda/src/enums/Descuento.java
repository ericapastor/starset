package enums;

import java.time.LocalDate;

/**
 * @author erica
 *
 */

public enum Descuento {
	NINGUNO, PRIMAVERA, NAVIDAD, SAN_VALENTIN;

	public String descuento() {
		switch (this) {
		case NINGUNO:
			return "Ninguno";
		case PRIMAVERA:
			return "Primavera";
		case NAVIDAD:
			return "Navidad";
		case SAN_VALENTIN:
			return "San Valentin";
		default:
			return null;
		}
	}

	/**
	 * Segun el dia actual se auto calcula un descuento que se aplicara a los
	 * productos. Este metodo entra en el constructor de Precio.
	 * 
	 * Del 14 de enero al 15 de febrero es el descuento de San Valentin
	 * 
	 * Del 15 de marzo al 30 de abril es el descuento de Primavera
	 * 
	 * Del 10 de noviembre al 26 de diciembre es el descuento de Navidad
	 * 
	 * @return Descuento descuento
	 * 
	 */
	public Descuento calcularDescuento() {
		// descuento de san valentin del 14 de enero al 15 de febrero
		if (LocalDate.now().getDayOfYear() > 14 && LocalDate.now().getDayOfYear() < 46) {
			return Descuento.SAN_VALENTIN;
			// descuento de privamera del 15 de marzo al 30 de abril
		} else if (LocalDate.now().getDayOfYear() > 74 && LocalDate.now().getDayOfYear() < 121) {
			return Descuento.PRIMAVERA;
			// descuento de navidad del 10 de noviembre al 26 de diciembre
		} else if (LocalDate.now().getDayOfYear() > 313 && LocalDate.now().getDayOfYear() < 361) {
			return Descuento.NAVIDAD;
		} else {
			return Descuento.NINGUNO;
		}
	}
}
