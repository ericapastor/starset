package enums;

/**
 * @author erica
 *
 */

public enum TipoBaraja {
	ORACULO, TAROT;

	public String baraja() {
		switch (this) {
		case ORACULO:
			return "Oraculo";
		case TAROT:
			return "Tarot";
		default:
			return "Indefinido";
		}
	}
}
