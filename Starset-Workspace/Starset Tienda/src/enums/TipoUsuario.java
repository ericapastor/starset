package enums;

public enum TipoUsuario {
	ADMINISTRADOR, CLIENTE;

	public String user() {
		switch (this) {
		case ADMINISTRADOR:
			return "administrador";
		case CLIENTE:
			return "cliente";
		default:
			return null;
		}
	}
}
