package enums;

public enum TipoProducto {
	NINGUNO, BARAJA, CRISTAL, VELA;

	public String tipoP() {
		switch (this) {
		case NINGUNO:
			return "Ninguno";
		case BARAJA:
			return "Baraja";
		case VELA:
			return "Vela";
		case CRISTAL:
			return "Cristal";
		default:
			return null;
		}
	}
}
