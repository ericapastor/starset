package enums;

public enum Signos {

	ARIES, TAURO, GEMINIS, CANCER, LEO, VIRGO, LIBRA, ESCORPIO, SAGITARIO, CAPRICORNIO, ACUARIO, PISCIS;

	public String getSignos() {
		switch (this) {
		case ARIES:
			return "aries";
		case TAURO:
			return "tauro";
		case GEMINIS:
			return "geminis";
		case CANCER:
			return "cancer";
		case LEO:
			return "leo";
		case VIRGO:
			return "virgo";
		case LIBRA:
			return "libra";
		case ESCORPIO:
			return "escorpio";
		case SAGITARIO:
			return "sagitario";
		case CAPRICORNIO:
			return "capricornio";
		case ACUARIO:
			return "acuario";
		case PISCIS:
			return "piscis";
		default:
			return null;
		}
	}

}
