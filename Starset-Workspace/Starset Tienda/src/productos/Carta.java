package productos;

import java.io.Serializable;

import programa.Programa;

/**
 * La clase Carta compone la clase Baraja. Las cartas tienen sus propios
 * atributos:
 * 
 * nombre: cada carta tiene uno independiente del nombre de la baraja a la que
 * pertenezca
 * 
 * interpretacion1: es la interpretacion que tiene la carta si en una tirada
 * sale del derecho
 * 
 * interpretacion2: es la interpretacion si en una tirada la carta sale del
 * reves
 * 
 * consejo: es la interpretacion de lo que la carta aconseja
 * 
 * delDerecho: boolean que indicara si la carta sale del derecho o del reves,
 * por defecto estara del derecho
 * 
 * @author erica
 *
 */

public class Carta implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nombre;
	private String interpretacion1;
	private String interpretacion2;
	private String consejo;
	private String elemento;
	private boolean delDerecho = true;

	/*
	 * *****************************************************************************
	 * ***************************** Constructor/es ********************************
	 * *****************************************************************************
	 */

	/**
	 * 
	 * Unico constructor de carta. Recibe sus cinco atributos necesarios para la
	 * creacion de la carta.
	 * 
	 * No hay mas constructores porque no tengo necesidad de crear objetos vacios.
	 * 
	 * @param nombre                   String
	 * @param interpretacionDelDerecho String
	 * @param interpretacionDelReves   String
	 * @param elemento                 String
	 * @param consejo                  String
	 */
	public Carta(String nombre, String elemento, String interpretacionDelDerecho, String interpretacionDelReves,
			String consejo) {
		this.nombre = nombre;
		this.elemento = elemento;
		this.interpretacion1 = interpretacionDelDerecho;
		this.interpretacion2 = interpretacionDelReves;
		this.consejo = consejo;
	}

	/*
	 * *****************************************************************************
	 * ***************************** Getters&Setters *******************************
	 * *****************************************************************************
	 */

	/**
	 * @return nombre String
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombreCarta String
	 */
	public void setNombre(String nombreCarta) {
		this.nombre = nombreCarta;
	}

	/**
	 * @return interpretacion1 String
	 */
	public String getInterpretacion1() {
		return interpretacion1;
	}

	/**
	 * @param interpretacion1 String
	 */
	public void setInterpretacion1(String interpretacion1) {
		this.interpretacion1 = interpretacion1;
	}

	/**
	 * @return interpretacion2 String
	 */
	public String getInterpretacion2() {
		return interpretacion2;
	}

	/**
	 * @param interpretacion2 String
	 */
	public void setInterpretacion2(String interpretacion2) {
		this.interpretacion2 = interpretacion2;
	}

	/**
	 * @return consejo String
	 */
	public String getConsejo() {
		return consejo;
	}

	/**
	 * @param consejo String
	 */
	public void setConsejo(String consejo) {
		this.consejo = consejo;
	}

	/**
	 * @return elemento String
	 */
	public String getElemento() {
		return elemento;
	}

	/**
	 * @param elemento String
	 */
	public void setElemento(String elemento) {
		this.elemento = elemento;
	}

	/**
	 * @return delDerecho boolean
	 */
	public boolean isDelDerecho() {
		return delDerecho;
	}

	/**
	 * @param delDerecho boolean
	 */
	public void setDelDerecho(boolean delDerecho) {
		this.delDerecho = delDerecho;
	}

	/*
	 * *****************************************************************************
	 * ******************************* METODOS *************************************
	 * *****************************************************************************
	 */

	/**
	 * toString
	 */
	@Override
	public String toString() {
		System.out.println("|           Nombre de la carta: " + Programa.toCamelCase(nombre)
				+ "\n|           Elemento: " + elemento + "\n|           Direccion de la carta: "
				+ (delDerecho ? "del derecho" : "del reves") + "\n|           Interpretacion:");
		if (delDerecho) {
			this.partirInterpretacion(interpretacion1);
		} else {
			this.partirInterpretacion(interpretacion2);
		}
		System.out.println("|           Consejito de la carta:");
		this.partirInterpretacion(consejo);
		return "";
	}

	/**
	 * Recibe una cadena String y la parte usando .split en un array para obtener
	 * los diferentes conceptos de la cadena de texto en un array. Luego los va
	 * mostrando capitalizando la primera letra y poniendoles un asterisco delante
	 * usando un bucle for each.
	 * 
	 * @param interpretacion String
	 * 
	 */
	private void partirInterpretacion(String interpretacion) {
		String[] partes = interpretacion.split(",");
		for (String s : partes) {
			System.out.println("|           * " + Programa.toCamelCase(s));
		}
	}

}
