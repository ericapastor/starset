package productos;

import java.io.Serializable;

import enums.TipoBaraja;
import enums.TipoProducto;
import programa.Pintar;
import programa.Programa;

/**
 * 
 * Baraja tiene un atributo enum que se refiere al tipo de baraja (oraculo o
 * tarot), y otro atributo de cartas, en el que recibe un array de objetos de la
 * clase Carta. La razon de usar un array y no un ArrayList en este caso es que
 * una baraja siempre tendra un numero de cartas definido, y no vamos a darlas
 * de alta por teclado. No hay necesidad de crear un objeto ArrayList.
 * 
 * Ademas tiene un magic number que es un final estatico (NUM_BAR_BASE) que
 * definira el numero de barajas a la hora de rellenarlas. Si tuviera que hacer
 * otra baraja, aumentaria ese numero sin necesidad de tener que cambiarlo en
 * todos los demas sitios del programa.
 * 
 * Es una clase final porque con ella termina la herencia (no tiene clases por
 * debajo suya jerarquicamente hablando).
 * 
 * @author erica
 * 
 */
public final class Baraja extends Producto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Carta[] cartas;
	private TipoBaraja tipo;

	/*
	 * *****************************************************************************
	 * ***************************** Constructor/es ********************************
	 * *****************************************************************************
	 */

	/**
	 * Constructor para crear barajas que usa el constructor vacio de Producto. No
	 * tengo necesidad de mas constructores.
	 */
	public Baraja() {
		super();
		iva = 15F;
	}

	/**
	 * 
	 * @param nombre String
	 * @param peso   float
	 * @param precio float
	 * @param n      int
	 * @param tipoB  TipoBaraja
	 * @param tipoP  TipoProducto
	 */
	public Baraja(String nombre, float peso, float precio, TipoProducto tipoP, int n, TipoBaraja tipoB) {
		super(nombre, peso, precio, tipoP);
		this.cartas = new Carta[n];
		this.tipo = tipoB;

	}

	/*
	 * *****************************************************************************
	 * ***************************** Getters&Setters *******************************
	 * *****************************************************************************
	 */

	/**
	 * @return cartasCarta[]
	 */
	public Carta[] getCartas() {
		return cartas;
	}

	/**
	 * @param cartas Carta[]
	 */
	public void setCartas(Carta[] cartas) {
		this.cartas = cartas;
	}

	/**
	 * @return tipo TipoBaraja
	 */
	public TipoBaraja getTipoBaraja() {
		return tipo;
	}

	/**
	 * @param tipo TipoBaraja
	 */
	public void setTipoBaraja(TipoBaraja tipo) {
		this.tipo = tipo;
	}

	/*
	 * *****************************************************************************
	 * ******************************* METODOS *************************************
	 * *****************************************************************************
	 */

	@Override
	public String toString() {
		return super.toString() + "|           Tipo de baraja: " + tipo.baraja() + "\n|           Numero de cartas: "
				+ cartas.length + "\n|           IVA aplicado: " + iva + "%\n|           Descuento aplicado: "
				+ precio.getDescuento() + "\n|";
	}

	/**
	 * Compare to que compara segun el codigo de producto
	 */
	@Override
	public int compareTo(Producto o) {
		return codigo.compareTo(o.codigo);
	}

	/**
	 * Calcular el precio de un producto dependiendo del descuento que se le
	 * aplique. Si no coincide con ninguno, solo se le aplica el IVA. Para baraja el
	 * descuento de primavera es del 5.55%, el de Navidad del 7.12% y el de San
	 * Valentin del 8.57%.
	 */
	@Override
	public void calcularPrecioNeto() {
		float precioB = this.getPrecio().getPrecioBruto();
		float precioN;
		switch (this.precio.getDescuento()) {
		case PRIMAVERA:
			precioN = precioB * (1 - iva / 100) * (1 + 5.55F / 100);
		case NAVIDAD:
			precioN = precioB * (1 - iva / 100) * (1 + 7.12F / 100);
		case SAN_VALENTIN:
			precioN = precioB * (1 - iva / 100) * (1 + 8.57F / 100);
		default:
			precioN = precioB * (1 - iva / 100);
		}
		this.precio.setPrecioNeto(precioN);
	}

	/**
	 * Da de alta una carta dentro del array de cartas de esta clase Baraja. Va
	 * comprobando si es la ultima carta o no, para avisar cuando se acaben.
	 * 
	 * @param nombre   String
	 * @param elemento String
	 * @param int1     String
	 * @param int2     String
	 * @param consejo  String
	 */
	private void altaCarta(String nombre, String elemento, String int1, String int2, String consejo) {
		boolean quedanCartas = false;
		for (int i = 0; i < cartas.length; i++) {
			if (i != cartas.length - 1) {
				quedanCartas = true;
			}
			if (cartas[i] == null) {
				cartas[i] = new Carta(nombre, elemento, int1, int2, consejo);
				break;
			}
		}
		if (!quedanCartas) {
			System.out.println("No quedan mas cartas que rellenar.");
		}
	}

	/**
	 * Pide cada dato necesario para el alta de una carta y comprueba cada uno.
	 * 
	 * Al final del todo pide confirmacion, y en caso de ser positiva da el alta de
	 * la carta. Se repite hasta que el usario responda "NO" cuando se le pregunta
	 * si quiere insertar mas cartas.
	 * 
	 * Si en cualquier momento durante el proceso se escribe 0, el proceso se
	 * detiene.
	 * 
	 * @see Pintar#datosAltas()
	 * @see Pintar#altaCartaNum(int)
	 * @see Pintar#pedirNombreProducto()
	 * @see Pintar#pedirElemento()
	 * @see Pintar#pedirInterpretacion(int)
	 * @see Pintar#pedirConsejo()
	 * @see Pintar#altaCancelada()
	 * @see Pintar#confirmacionCarta(String, String, String, String, String)
	 * @see Pintar#intentaloDeNuevo()
	 * @see Pintar#repetirAltaCarta()
	 * @see Pintar#altasFinalizadas()
	 * @see Programa#pedirDatosString()
	 * @see Programa#pedirDatosBoolean()
	 * @see #altaCarta(String, String, String, String, String)
	 */

	public void pedirDatosCartas() {
		boolean confirmacion = true;
		int i = 0;
		Pintar.datosAltas();
		while (confirmacion) {
			Pintar.altaCartaNum(i);
			Pintar.pedirNombreProducto();
			String nombre = Programa.pedirDatosString();
			if (nombre.equals("0")) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirElemento();
			String elemento = Programa.pedirDatosString();
			if (elemento.equals("0")) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirInterpretacion(0);
			String int1 = Programa.pedirDatosString();
			if (int1.equals("0")) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirInterpretacion(1);
			String int2 = Programa.pedirDatosString();
			if (int2.equals("0")) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirConsejo();
			String consejo = Programa.pedirDatosString();
			if (consejo.equals("0")) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.confirmacionCarta(nombre, elemento, int1, int2, consejo);
			confirmacion = Programa.pedirDatosBoolean();
			if (!confirmacion) {
				Pintar.altaCancelada();
				Pintar.intentaloDeNuevo();
				confirmacion = true;
			} else {
				altaCarta(nombre, elemento, int1, int2, consejo);
				i++;
				Pintar.repetirAltaCarta();
				confirmacion = Programa.pedirDatosBoolean();
				if (!confirmacion) {
					Pintar.altasFinalizadas();
				}
			}
		}

	}

	/**
	 * Seleccionar un numero entre las posiciones del array de cartas.
	 * 
	 * @return carta int
	 * 
	 */
	public int elegirCartaAleatoria() {
		int carta = (int) (Math.round(Math.random() * this.cartas.length));
		if (carta >= this.cartas.length) {
			carta = cartas.length - 1;
		} else if (carta < 0) {
			carta = 0;
		}
		return carta;
	}

	/**
	 * Seleccionar 0 o 1 para posteriormente saber si una carta sale del derecho o
	 * del reves.
	 * 
	 * @return posicion boolean
	 * 
	 */
	public boolean elegirPosicionAleatoria() {
		int posicion = (int) (Math.round(Math.random() * 2));
		if (posicion > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Elegir tres cartas del derecho o del reves aleatoriamente y luego mostrarlas.
	 * 
	 * Este metodo controla excepciones de fuera de rango
	 * (StringIndexOutOfBoundsException) y de NullPointer
	 * 
	 * @see Pintar#cartaPasado()
	 * @see Pintar#cartaPresente()
	 * @see Pintar#cartaFuturo()
	 * @see #elegirCartaAleatoria()
	 * @see #elegirPosicionAleatoria()
	 */
	public void lecturaTresCartas() {
		Pintar.cartaPasado();
		try {
			for (int i = 0; i < 3; i++) {
				Carta carta = cartas[elegirCartaAleatoria()];
				carta.setDelDerecho(elegirPosicionAleatoria());
				System.out.println(carta);
			}
		} catch (java.lang.StringIndexOutOfBoundsException e) {
			Pintar.algoFueMal();
		} catch (java.lang.NullPointerException n) {
			Pintar.noHayCartas();
		}
	}
}
