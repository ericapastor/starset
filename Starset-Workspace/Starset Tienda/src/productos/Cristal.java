package productos;

import java.io.Serializable;

import enums.TipoProducto;

/**
 * Cristal tiene un static final para definir el numero de cristales base que se
 * van a dar de alta, y un contador estatico numCristales que servira para auto
 * calcular el codigo de producto.
 * 
 * Ademas tiene un atributo boolean que se refiere a si el cristal esta pulido o
 * no, y otro String que se refiere a la forma del cristal.
 * 
 * Es una clase final porque con ella termina la herencia (no tiene hijos).
 * 
 * @author erica
 *
 */

public final class Cristal extends Producto implements Serializable {

	private static final long serialVersionUID = 1L;

	private boolean pulido;
	private String forma;

	/*
	 * *****************************************************************************
	 * ***************************** Constructor/es ********************************
	 * *****************************************************************************
	 */

	/**
	 * Constructor vacio
	 */
	public Cristal() {
		super();
		iva = 6.66F;
	}

	/**
	 * Constructor con todos los parametros que usa el constructor de tres atributos
	 * de la clase Producto.
	 * 
	 * @param nombre String
	 * @param peso   float
	 * @param precio float
	 * @param pulido boolean
	 * @param forma  String
	 * @param tipoP    TipoProducto
	 */
	public Cristal(String nombre, float peso, float precio, TipoProducto tipoP, boolean pulido, String forma) {
		super(nombre, peso, precio, tipoP);
		this.pulido = pulido;
		this.forma = forma;
		iva = 6.66F;
	}

	/*
	 * *****************************************************************************
	 * ***************************** Getters&Setters *******************************
	 * *****************************************************************************
	 */

	/**
	 * @return pulido boolean
	 */
	public boolean isPulido() {
		return pulido;
	}

	/**
	 * @param pulido boolean
	 */
	public void setPulido(boolean pulido) {
		this.pulido = pulido;
	}

	/**
	 * @return forma String
	 */
	public String getForma() {
		return forma;
	}

	/**
	 * @param forma String
	 */
	public void setForma(String forma) {
		this.forma = forma;
	}

	/*
	 * *****************************************************************************
	 * ******************************* METODOS *************************************
	 * *****************************************************************************
	 */

	@Override
	public String toString() {
		return super.toString() + "|           Pulido: " + (pulido ? "Si" : "No") + "\n|           Forma: " + forma
				+ "\n|           IVA aplicado: " + iva + "%\n|           Descuento aplicado: " + precio.getDescuento()
				+ "\n|";
	}

	/**
	 * Compare to que compara segun el codigo de producto
	 */
	@Override
	public int compareTo(Producto o) {
		return codigo.compareTo(o.codigo);
	}

	/**
	 * Calcular el precio de un producto dependiendo del descuento que se le
	 * aplique. Si no coincide con ninguno, solo se le aplica el IVA. Para baraja el
	 * descuento de primavera es del 1.2%, el de Navidad del 3.3% y el de San
	 * Valentin del 2.78%.
	 */
	@Override
	public void calcularPrecioNeto() {
		float precioB = this.precio.getPrecioBruto();
		float precioN;
		switch (this.precio.getDescuento()) {
		case PRIMAVERA:
			precioN = precioB * (1 - iva / 100) * (1 + 1.2F / 100);
		case NAVIDAD:
			precioN = precioB * (1 - iva / 100) * (1 + 3.3F / 100);
		case SAN_VALENTIN:
			precioN = precioB * (1 - iva / 100) * (1 + 2.78F / 100);
		default:
			precioN = precioB * (1 - iva / 100);
		}
		this.precio.setPrecioNeto(precioN);
	}

}
