package productos;

import java.io.Serializable;

import enums.AromaVela;
import enums.TipoProducto;

/**
 * Vela tiene un contador estatico numVelas para auto calcular el codigo de
 * producto.
 * 
 * Ademas tiene los atributos String color, float diametro, float altura e int
 * unidades.
 * 
 * Las unidades hacen referencia a cuantas hay de ese tipo de vela.
 * 
 * Por ultimo tiene un atributo del Enum AromaVela, y segun el aroma que una
 * vela reciba, tendra un nombre y un color pre-definidos.
 * 
 * Es una clase final porque con ella termina la herencia (no tiene hijos).
 * 
 * @author erica
 *
 */

public final class Vela extends Producto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String color;
	private float diametro;
	private float altura;
	private int unidades;
	private AromaVela aroma;

	/*
	 * *****************************************************************************
	 * ***************************** Constructor/es ********************************
	 * *****************************************************************************
	 */

	/**
	 * Constructor vacio
	 */
	public Vela() {
		super();
		iva = 10.7F;
	}

	/**
	 * Aqui Vela usa el constructor que no recibe nombre de la clase Producto (el de
	 * dos atributos).
	 * 
	 * @param peso     float
	 * @param precio   float
	 * @param diametro float
	 * @param altura   float
	 * @param unidades int
	 * @param aroma    AromaVela
	 * @param tipoP    TipoProducto
	 * 
	 */
	public Vela(float peso, float precio, TipoProducto tipoP, float diametro, float altura, int unidades,
			AromaVela aroma) {
		super(peso, precio, tipoP);
		this.diametro = diametro;
		this.altura = altura;
		this.unidades = unidades;
		this.aroma = aroma;
		nombre = aroma.nombre();
		color = aroma.color();
		iva = 10.7F;
	}

	/*
	 * *****************************************************************************
	 * ***************************** Getters&Setters *******************************
	 * *****************************************************************************
	 */

	/**
	 * @return color String
	 */
	public String getColor() {
		return color;
	}

	/**
	 * @param color String
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return diametro float
	 */
	public float getDiametro() {
		return diametro;
	}

	/**
	 * @param diametro float
	 */
	public void setDiametro(float diametro) {
		this.diametro = diametro;
	}

	/**
	 * @return altura float
	 */
	public float getAltura() {
		return altura;
	}

	/**
	 * @param altura float
	 */
	public void setAltura(float altura) {
		this.altura = altura;
	}

	/**
	 * @return unidades int
	 */
	public int getUnidades() {
		return unidades;
	}

	/**
	 * @param unidades int
	 */
	public void setUnidades(int unidades) {
		this.unidades = unidades;
	}

	/**
	 * @return aroma AromaVela
	 */
	public AromaVela getAroma() {
		return aroma;
	}

	/**
	 * @param aroma AromaVela
	 */
	public void setAroma(AromaVela aroma) {
		this.aroma = aroma;
	}

	/*
	 * *****************************************************************************
	 * ******************************* METODOS *************************************
	 * *****************************************************************************
	 */

	@Override
	public String toString() {
		return super.toString() + "|           Aroma: " + aroma.aroma() + "\n|           Color: " + color
				+ "\n|           Medidas: " + diametro + " cm de diametro y " + altura
				+ " cm de altura\n|           Unidades disponibles: " + unidades + "\n|           Peso: " + peso
				+ "g\n|           IVA aplicado: " + iva + "%\n|           Descuento aplicado: " + precio.getDescuento()
				+ "\n|";
	}

	/**
	 * Compare to que compara segun el codigo de producto
	 */
	@Override
	public int compareTo(Producto o) {
		return codigo.compareTo(o.codigo);
	}

	/**
	 * Calcular el precio de un producto dependiendo del descuento que se le
	 * aplique. Si no coincide con ninguno, solo se le aplica el IVA. Para vela el
	 * descuento de primavera es del 2.10%, el de Navidad del 2.76% y el de San
	 * Valentin del 7.5%.
	 */
	@Override
	public void calcularPrecioNeto() {
		float precioB = this.precio.getPrecioBruto();
		float precioN;
		switch (this.precio.getDescuento()) {
		case PRIMAVERA:
			precioN = precioB * (1 - iva / 100) * (1 + 2.1F / 100);
		case NAVIDAD:
			precioN = precioB * (1 - iva / 100) * (1 + 2.76F / 100);
		case SAN_VALENTIN:
			precioN = precioB * (1 - iva / 100) * (1 + 7.5F / 100);
		default:
			precioN = precioB * (1 - iva / 100);
		}
		this.precio.setPrecioNeto(precioN);
	}

}