package productos;

import java.io.Serializable;

import enums.Descuento;

/**
 * La clase Precio ser un objeto de la clase Producto. Tiene un Enum de
 * Descuento, un float como precio bruto y otro float como precio neto.
 * 
 * El descuento se calculara en el constructor de Precio automaticamente cada
 * vez que se cree un nuevo objeto, y dependera de la fecha del dia de hoy.
 * 
 * El precio bruto sera rellenado por teclado o manualmente.
 * 
 * El precio neto sera calculado por cada producto dependiendo si tiene
 * descuento y de su IVA propio.
 * 
 * @author erica
 *
 */

public class Precio implements Serializable {

	private static final long serialVersionUID = 1L;

	private Descuento descuento;
	private float precioBruto;
	private float precioNeto;

	/*
	 * *****************************************************************************
	 * ***************************** Constructor/es ********************************
	 * *****************************************************************************
	 */

	/**
	 * Constructor vacio que inicializa un precio a 0 y calcular el descuento de la
	 * epoca actual.
	 */
	public Precio() {
		precioBruto = 0.0F;
		descuento = descuento.calcularDescuento();
	}

	/**
	 * Constructor con el unico atributo que puede recibir, que calcula el descuento
	 * que de la epoca actual.
	 * 
	 * @param precio float
	 */
	public Precio(float precio) {
		precioBruto = precio;
		descuento = descuento.calcularDescuento();
	}

	/*
	 * *****************************************************************************
	 * ***************************** Getters&Setters *******************************
	 * *****************************************************************************
	 */

	/**
	 * @return descuento Descuento
	 */
	public Descuento getDescuento() {
		return descuento;
	}

	/**
	 * @param descuento Descuento
	 */
	public void setDescuento(Descuento descuento) {
		this.descuento = descuento;
	}

	/**
	 * @return precioBruto float
	 */
	public float getPrecioBruto() {
		return precioBruto;
	}

	/**
	 * @param precioBruto float
	 */
	public void setPrecioBruto(float precioBruto) {
		this.precioBruto = precioBruto;
	}

	/**
	 * @return precioNeto float
	 */
	public float getPrecioNeto() {
		return precioNeto;
	}

	/**
	 * @param precioNeto float
	 */
	public void setPrecioNeto(float precioNeto) {
		this.precioNeto = precioNeto;
	}

	/*
	 * Esta clase no tiene toString ni metodos
	 */

}
