package productos;

import java.io.Serializable;

import enums.TipoProducto;

/**
 * Producto es una clase abstracta que inicia una herencia de producto: Barajas,
 * Velas y Cristales. De aqui estas clases heredan los atributos de nombre,
 * peso, precio, iva y codigo.
 * 
 * El precio sera un objeto de clase Precio con sus propios atributos.
 * 
 * El iva sera redefinido (sobreescrito) en cada constructor de cada hijo. Su
 * valor por defecto es del 21%.
 * 
 * El codigo sera auto calculado cada vez que se haga un alta de un producto
 * segun el tipo de producto y el contador de productos propio de cada clase.
 * 
 * @author erica
 *
 */

public abstract class Producto implements Comparable<Producto>, Serializable {

	private static final long serialVersionUID = 1L;

	String nombre;
	float peso;
	Precio precio;
	float iva = 21F;
	String codigo;
	TipoProducto tipo;

	/*
	 * *****************************************************************************
	 * ***************************** Constructor/es ********************************
	 * *****************************************************************************
	 */

	/**
	 * Constructor vacio que instancia el atributo Precio precio vacio, utilizando
	 * el constructor de Precio que no recibe atributos. Sera utilizado en el alta
	 * de barajas. Tambien inicializa el tipo de producto a ninguno.
	 * 
	 */
	public Producto() {
		precio = new Precio();
		tipo = TipoProducto.NINGUNO;
	}

	/**
	 * Constructor que recibe dos atributos, instanciando precio con el constructor
	 * que recibe un atributo. Sera utilizado en el alta de velas, dado que las
	 * velas tienen su propio nombre asignado segun el aroma.
	 * 
	 * @param peso   String
	 * @param precio float
	 * @param tipo  TipoProducto
	 * 
	 */
	public Producto(float peso, float precio, TipoProducto tipo) {
		this.peso = peso;
		this.precio = new Precio(precio);
		this.tipo = tipo;
	}

	/**
	 * Constructor que recibe tres atributos, instanciando precio con el constructor
	 * que recibe un atributo. Sera utilizado en el alta de cristales.
	 * 
	 * @param nombre String
	 * @param peso   float
	 * @param precio float
	 * @param tipo  TipoProducto
	 * 
	 */
	public Producto(String nombre, float peso, float precio, TipoProducto tipo) {
		this.nombre = nombre;
		this.peso = peso;
		this.precio = new Precio(precio);
		this.tipo = tipo;
	}

	/*
	 * *****************************************************************************
	 * ***************************** Getters&Setters *******************************
	 * *****************************************************************************
	 */

	/**
	 * @return nombre String
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre String
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return peso float
	 */
	public float getPeso() {
		return peso;
	}

	/**
	 * @param peso float
	 */
	public void setPeso(float peso) {
		this.peso = peso;
	}

	/**
	 * @return precio Precio
	 */
	public Precio getPrecio() {
		return precio;
	}

	/**
	 * @param precio Precio
	 */
	public void setPrecio(Precio precio) {
		this.precio = precio;
	}

	/**
	 * @return iva float
	 */
	public float getIVA() {
		return iva;
	}

	/**
	 * @return codigo String
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo String
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public TipoProducto getTipo() {
		return tipo;
	}

	public void setTipo(TipoProducto tipo) {
		this.tipo = tipo;
	}

	/*
	 * *****************************************************************************
	 * ******************************* METODOS *************************************
	 * *****************************************************************************
	 */

	/**
	 * toString
	 */
	@Override
	public String toString() {
		return "\n|           Nombre: " + this.nombre + "\n|           Codigo " + this.codigo + "\n|           Peso: "
				+ this.peso + "\n|           Precio bruto: " + this.precio.getPrecioBruto()
				+ "\n|           Precio neto: " + this.precio.getPrecioNeto() + "\n";
	}

	/**
	 * Calcular el precio dependiendo del descuento aplicado y del IVA.
	 */
	abstract void calcularPrecioNeto();

}
