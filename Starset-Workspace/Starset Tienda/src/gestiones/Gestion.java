package gestiones;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import clientes.User;
import clientes.Pedido;
import enums.AromaVela;
import enums.TipoBaraja;
import enums.TipoProducto;
import enums.TipoUsuario;
import lectura.Signo;
import productos.Baraja;
import productos.Cristal;
import productos.Producto;
import productos.Vela;
import programa.Pintar;
import programa.PintarMenus;
import programa.Programa;

/**
 * 
 * La clase GestionProductos tiene como atributos ArrayLists de productos. Esta
 * en un paquete diferente porque si en un futuro quiero crear mas clases de
 * gestiones, iran a este paquete.
 * 
 * Tiene un solo constructor vacio.
 * 
 * Tiene setters y getters.
 * 
 * El toString llama a metodos de abajo.
 * 
 * Tiene metodos de gestion: altas, bajas, modificaciones, visualizaciones y
 * busquedas.
 * 
 * @author erica
 *
 */

public class Gestion implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final String datosProductos = "src/datosProductos.dat";
	private static final String datosUsers = "src/datosUsers.dat";
	private static final String datosPedidos = "src/datosPedidos.dat";

	private ArrayList<Producto> productos;
	private ArrayList<User> users;
	private ArrayList<Pedido> pedidos;

	/*
	 * *****************************************************************************
	 * ***************************** Constructor/es ********************************
	 * *****************************************************************************
	 */

	/**
	 * Un solo constructor vacio
	 */
	public Gestion() {
		productos = new ArrayList<Producto>();
		users = new ArrayList<User>();
		pedidos = new ArrayList<Pedido>();
	}

	/*
	 * *****************************************************************************
	 * ***************************** Getters&Setters *******************************
	 * *****************************************************************************
	 */

	/**
	 * @return productos ArrayList(Producto)
	 */
	public ArrayList<Producto> getProductos() {
		return productos;
	}

	/**
	 * @param productos ArrayList(Producto)
	 */
	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}

	/**
	 * @return clientes ArrayList(Cliente)
	 */
	public ArrayList<User> getUsers() {
		return users;
	}

	/**
	 * @param clientes ArrayList(Cliente(
	 */
	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}

	/**
	 * @return ventas ArrayList(Venta)
	 */
	public ArrayList<Pedido> getPedidos() {
		return pedidos;
	}

	/**
	 * @param pedidos ArrayList(Venta)
	 */
	public void setPedidos(ArrayList<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	/*
	 * *****************************************************************************
	 * ********************************** METODOS **********************************
	 * *****************************************************************************
	 */

	/**
	 * 
	 * @see #listarBarajas()
	 * @see #listarVelas()
	 * @see #listarCristales()
	 * 
	 */
	@Override
	public String toString() {
		System.out.println("|           **********************************");
		System.out.println("|           BARAJAS:\n|");
		System.out.println("|           **********************************");
		listarProductos(1);
		System.out.println("|           **********************************");
		System.out.println("|           CRISTALES:\n|");
		System.out.println("|           **********************************");
		listarProductos(2);
		System.out.println("|           **********************************");
		System.out.println("|           VELAS:\n|");
		System.out.println("|           **********************************");
		listarProductos(3);
		return "|           **********************\n|\n|           Esos son todos nuestros productos.";
	}

	/*
	 * *****************************************************************************
	 * ****************************** -----ALTAS----- ******************************
	 * *****************************************************************************
	 */

	public void altaUsersBase() {
		Signo signo = new Signo();
		signo.calcularCartaAstral(LocalDate.parse("1995-02-13"), LocalTime.parse("17:00"));
		users.add(new User("CLI" + users.size(), "Erica Elisabeth", LocalDate.parse("1995-02-13"),
				LocalTime.parse("17:00"), signo, "effyelle", "123@miPwd", TipoUsuario.ADMINISTRADOR));
		signo.calcularCartaAstral(LocalDate.parse("2000-11-15"), LocalTime.parse("00:00"));
		users.add(new User("CLI" + users.size(), "Lady J La Pro", LocalDate.parse("2000-11-15"),
				LocalTime.parse("00:00"), signo, "leidi2000", "otra_2Pwd", TipoUsuario.ADMINISTRADOR));
		signo.calcularCartaAstral(LocalDate.parse("1998-01-04"), LocalTime.parse("21:00"));
		users.add(new User("CLI" + users.size(), "Michi michi miau", LocalDate.parse("1998-01-04"),
				LocalTime.parse("21:00"), signo, "98mich", "dori_Zzz123", TipoUsuario.ADMINISTRADOR));
		guardarDatosUsers();
	}

	/**
	 * Dar de alta una baraja, metodo que se usaria para dar de alta por teclado.
	 * 
	 * @param nombre String
	 * @param peso   float
	 * @param precio float
	 * @param n      int
	 * @param tipoB  TipoBaraja
	 * 
	 * @see #calcularCodigo(int)
	 * @see #ordenaListaPorCodigo()
	 * 
	 */
	private void altaBaraja(String nombre, float peso, float precio, int n, TipoBaraja tipoB) {
		Baraja b = new Baraja(nombre, peso, precio, TipoProducto.BARAJA, n, tipoB);
		productos.add(b);
		calcularCodigo(1);
		ordenaListaPorCodigo();
	}

	/**
	 * Recibe los seis atributos necesarios para dar de alta un cristal, y despues
	 * llama a calcular el precio neto, calcular el codigo y el control de contador
	 * de productos de Producto sobreescritos en la clase Cristal.
	 * 
	 * @param nombre String
	 * @param peso   float
	 * @param precio float
	 * @param codigo String
	 * @param pulido boolean
	 * @param forma  String
	 * 
	 * @see Cristal#calcularPrecioNeto()
	 * @see #calcularCodigo(int)
	 * @see #ordenaListaPorCodigo()
	 * 
	 */
	private void altaCristales(String nombre, float peso, float precio, boolean pulido, String forma) {
		Cristal c = new Cristal(nombre, peso, precio, TipoProducto.CRISTAL, pulido, forma);
		c.calcularPrecioNeto();
		productos.add(c);
		calcularCodigo(2);
		ordenaListaPorCodigo();
	}

	/**
	 * Este es el metodo que se utilizara en el alta de velas por teclado.
	 * 
	 * Dar de alta velas con sus parametros. Llama al metodo altaProducto
	 * sobreescrito en la clase Vela para asignar nombre y color, buscando
	 * coincidencias a traves del .values() en un bucle for. Tras encontrar la
	 * coincidencia, se rompe el bucle.
	 * 
	 * Llama a calcular el precio neto, calcular el codigo y el control de contador
	 * de productos de Producto sobreescritos en la clase Vela.
	 * 
	 * @param peso     float
	 * @param precio   float
	 * @param codigo   String
	 * @param diametro float
	 * @param altura   float
	 * @param unidades int
	 * @param aroma    AromaVela
	 * 
	 * @see Vela#calcularPrecioNeto()
	 * @see #calcularCodigo(int)
	 * @see #ordenaListaPorCodigo()
	 * 
	 */
	private void altaVelas(float peso, float precio, float diametro, float altura, int unidades, AromaVela aroma) {
		Vela v = new Vela(peso, precio, TipoProducto.VELA, diametro, altura, unidades, aroma);
		v.calcularPrecioNeto();
		productos.add(v);
		calcularCodigo(3);
		ordenaListaPorCodigo();
	}

	private void altaCliente(String n, LocalDate f, LocalTime h, Signo ct, String nick, String pd, TipoUsuario t) {
		users.add(new User("CLI" + users.size(), n, f, h, ct, nick, pd, t));
		ordenaListaPorCodigo();
	}

	private void altaPedido(String id, String f, User c) {
		pedidos.add(new Pedido("PED" + pedidos.size(), f, c));
		ordenaListaPorCodigo();
	}

	/*
	 * *****************************************************************************
	 * *************************** -----PEDIR DATOS----- ***************************
	 * *****************************************************************************
	 */

	/**
	 * Pide cada dato necesario para el alta de una baraja y comprueba cada uno.
	 * 
	 * Al final del todo pide confirmacion, y en caso de ser positiva da el alta de
	 * la baraja. Se repite hasta que el usario responda "NO" cuando se le pregunta
	 * si quiere insertar mas barajas.
	 * 
	 * Si en cualquier momento durante el proceso se escribe 0, el proceso se
	 * detiene.
	 * 
	 * @see Pintar#datosAltas()
	 * @see Pintar#altaBarajaNum(int)
	 * @see Pintar#pedirNombreProducto()
	 * @see Pintar#pedirPeso()
	 * @see Pintar#pedirPrecio()
	 * @see Pintar#pedirNumCartas()
	 * @see Pintar#pedirTipoBaraja()
	 * @see Pintar#altaCancelada()
	 * @see Pintar#confirmacionBaraja(String, float, float, int, String)
	 * @see Pintar#intentaloDeNuevo()
	 * @see Pintar#repetirAltaBaraja()
	 * @see Pintar#altasFinalizadas()
	 * @see Programa#pedirDatosString()
	 * @see Programa#pedirDatosFloat()
	 * @see Programa#pedirDatosBoolean()
	 * @see #buscarTipoBaraja(String)
	 * @see #altaBaraja(String, float, float, int, TipoBaraja)
	 * 
	 */
	public void pedirDatosBaraja() {
		boolean confirmacion = true;
		int i = 0;
		Pintar.datosAltas();
		while (confirmacion) {
			Pintar.altaProdNum(i);
			Pintar.pedirNombreProducto();
			String nombre = Programa.pedirDatosString();
			if (nombre.equals("0")) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirPeso();
			float peso = Programa.pedirDatosFloat();
			if (peso == 0F) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirPrecio();
			float precio = Programa.pedirDatosFloat();
			if (precio == 0F) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirNumCartas();
			int numCartas = Programa.pedirDatosInt();
			if (numCartas == 0) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirTipoBaraja();
			String t = "";
			TipoBaraja tipo = null;
			while (true) {
				t = Programa.pedirDatosString();
				if (t.equals("0")) {
					Pintar.altaCancelada();
					break;
				} else if (buscarTipoBaraja(t) != null) {
					tipo = buscarTipoBaraja(t);
					break;
				}
			}
			Pintar.confirmacionBaraja(nombre, peso, precio, numCartas, tipo.baraja());
			confirmacion = Programa.pedirDatosBoolean();
			if (!confirmacion) {
				Pintar.altaCancelada();
				Pintar.intentaloDeNuevo();
				confirmacion = true;
			} else {
				altaBaraja(nombre, peso, precio, numCartas, tipo);
				i++;
				Pintar.repetirAltaBaraja();
				confirmacion = Programa.pedirDatosBoolean();
				if (!confirmacion) {
					Pintar.altasFinalizadas();
				}
			}
		}
	}

	/**
	 * Pide cada dato necesario para el alta de una vela y los va comprobando uno
	 * por uno. Al final del todo pide confirmacion, y en caso de ser positiva da el
	 * alta de la vela.
	 * 
	 * Se repite hasta que el usario responda "NO" cuando se le pregunta si quiere
	 * insertar mas velas.
	 * 
	 * Si en cualquier momento durante el proceso se escribe 0, el proceso se
	 * detiene.
	 * 
	 * @see Pintar#datosAltas()
	 * @see Pintar#altaVelaNum(int)
	 * @see Pintar#pedirAromaVela()
	 * @see Pintar#pedirUnidadesVela()
	 * @see Pintar#pedirPrecio()
	 * @see Pintar#pedirPeso()
	 * @see Pintar#pedirDiametro()
	 * @see Pintar#pedirAltura()
	 * @see Pintar#altaCancelada()
	 * @see Pintar#confirmacionVela(String, int, float, float, float, float)
	 * @see Pintar#intentaloDeNuevo()
	 * @see Pintar#repetirAltaVela()
	 * @see Pintar#altasFinalizadas()
	 * @see Programa#toCamelCase(String)
	 * @see Programa#pedirDatosString()
	 * @see Programa#pedirDatosInt()
	 * @see Programa#pedirDatosFloat()
	 * @see Programa#pedirDatosBoolean()
	 * @see #buscarAroma(String)
	 * @see #altaVelas(float, float, float, float, int, AromaVela)
	 * 
	 */
	public void pedirDatosVelas() {
		boolean confirmacion = true;
		int i = 0;
		Pintar.datosAltas();
		while (confirmacion) {
			Pintar.altaProdNum(i);
			Pintar.pedirAromaVela();
			String a = "";
			AromaVela aroma = null;
			while (true) {
				a = Programa.pedirDatosString();
				if (a.equals("0")) {
					break;
				} else if (buscarAroma(a) != null) {
					aroma = buscarAroma(a);
					a = Programa.toCamelCase(a);
					break;
				}
			}
			if (a.equals("0")) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirUnidadesVela();
			int unidades = Programa.pedirDatosInt();
			if (unidades == 0) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirPrecio();
			float precio = Programa.pedirDatosFloat();
			if (precio == 0F) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirPeso();
			float peso = Programa.pedirDatosFloat();
			if (peso == 0F) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirDiametro();
			float diametro = Programa.pedirDatosFloat();
			if (diametro == 0F) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirAltura();
			float altura = Programa.pedirDatosFloat();
			if (altura == 0F) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.confirmacionVela(a, unidades, precio, peso, diametro, altura);
			confirmacion = Programa.pedirDatosBoolean();
			if (!confirmacion) {
				Pintar.altaCancelada();
				Pintar.intentaloDeNuevo();
				confirmacion = true;
			} else {
				altaVelas(peso, precio, diametro, altura, unidades, aroma);
				i++;
				Pintar.repetirAltaVela();
				confirmacion = Programa.pedirDatosBoolean();
				if (!confirmacion) {
					Pintar.altasFinalizadas();
				}
			}
		}

	}

	/**
	 * Pide cada dato necesario para el alta de un cristal y comprueba cada uno.
	 * 
	 * Al final del todo pide confirmacion, y en caso de ser positiva da el alta del
	 * cristal. Se repite hasta que el usario responda "NO" cuando se le pregunta si
	 * quiere insertar mas cristales.
	 * 
	 * Si en cualquier momento durante el proceso se escribe 0, el proceso se
	 * detiene.
	 * 
	 * @see Pintar#datosAltas()
	 * @see Pintar#altaProdNum(int)
	 * @see Pintar#pedirNombreProducto()
	 * @see Pintar#pedirPeso()
	 * @see Pintar#pedirPrecio()
	 * @see Pintar#pedirPulido()
	 * @see Pintar#altaCancelada()
	 * @see Pintar#confirmacionCristal(String, float, float, boolean, String)
	 * @see Pintar#intentaloDeNuevo()
	 * @see Pintar#repetirAltaCristal()
	 * @see Pintar#altasFinalizadas()
	 * @see Programa#pedirDatosString()
	 * @see Programa#pedirDatosFloat()
	 * @see Programa#pedirDatosBoolean()
	 * @see #altaCristales(String, float, float, boolean, String)
	 * 
	 */
	public void pedirDatosCristal() {
		boolean confirmacion = true;
		int i = 0;
		Pintar.datosAltas();
		while (confirmacion) {
			Pintar.altaProdNum(i);
			Pintar.pedirNombreProducto();
			String nombre = Programa.pedirDatosString();
			if (nombre.equals("0")) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirPeso();
			float peso = Programa.pedirDatosFloat();
			if (peso == 0F) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirPrecio();
			float precio = Programa.pedirDatosFloat();
			if (precio == 0F) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirPulido();
			boolean isPulido = Programa.pedirDatosBoolean();
			Pintar.pedirForma();
			String forma = Programa.pedirDatosString();
			if (forma.equals("0")) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.confirmacionCristal(nombre, peso, precio, isPulido, forma);
			confirmacion = Programa.pedirDatosBoolean();
			if (!confirmacion) {
				Pintar.altaCancelada();
				Pintar.intentaloDeNuevo();
				confirmacion = true;
			} else {
				altaCristales(nombre, peso, precio, isPulido, forma);
				i++;
				Pintar.repetirAltaCristal();
				confirmacion = Programa.pedirDatosBoolean();
				if (!confirmacion) {
					Pintar.altasFinalizadas();
				}
			}
		}
	}

	/**
	 * Pide los datos para un alta de cliente y los va comprobando
	 * 
	 * @see #guardarDatosUsers()
	 * @see Pintar#altaClienteNum(int)
	 * @see Pintar#pedirNombreCliente()
	 * @see Pintar#altaCancelada()
	 * @see Pintar#pedirPasswd()
	 * @see Pintar#pedirFechaNacimiento()
	 * @see Pintar#pedirHoraNacimiento()
	 * @see Pintar#algoFueMal()
	 * @see Pintar#confirmacionCliente(String, String, LocalDate, LocalTime)
	 * @see Pintar#intentaloDeNuevo()
	 * @see Pintar#repetirAltaCliente()
	 * @see Pintar#altasFinalizadas()
	 * @see Programa#pedirDatosString()
	 * @see Programa#pedirPasswd()
	 * @see Programa#pedirDatosFecha()
	 * @see Programa#pedirDatosHora()
	 * @see Programa#pedirDatosBoolean()
	 * @see Signo#calcularCartaAstral(LocalDate, LocalTime)
	 * @see #altaCliente(String, LocalDate, LocalTime, Signo, String, boolean,
	 *      TipoUsuario)
	 * 
	 */
	public void pedirDatosCliente() {
		Pintar.datosAltas();
		while (true) {
			Pintar.pedirNombreCliente();
			String nombre = Programa.pedirDatosString();
			if (nombre.equals("0")) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirNickname();
			String nick = this.pedirNickname();
			if (nick.equals("0")) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirPasswd();
			String passwd = Programa.pedirPasswd();
			if (passwd.equals("0")) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirFechaNacimiento();
			LocalDate fecha = Programa.pedirDatosFecha();
			if (fecha == null) {
				Pintar.altaCancelada();
				break;
			}
			Pintar.pedirHoraNacimiento();
			LocalTime hora = Programa.pedirDatosHora();
			if (hora == null) {
				Pintar.altaCancelada();
				break;
			}
			Signo carta = new Signo();
			carta.calcularCartaAstral(fecha, hora);
			if (carta.getSolar() == null || carta.getLunar() == null || carta.getAscendente() == null) {
				Pintar.algoFueMal();
				break;
			} else {
				Pintar.confirmacionCliente(nombre, passwd, fecha, hora);
				if (!Programa.pedirDatosBoolean()) {
					Pintar.altaCancelada();
					Pintar.intentaloDeNuevo();
				} else {
					// solo se pueden dar de alta por aqui como clientes
					// nunca como administradores
					altaCliente(nombre, fecha, hora, carta, nick, passwd, TipoUsuario.CLIENTE);
					guardarDatosUsers();
					break;
				}
			}

		}
	}

	/*
	 * *****************************************************************************
	 * **************************** -----CALCULOS----- *****************************
	 * *****************************************************************************
	 */

	/**
	 * Metodo para quitar el try catch de null pointer del metodo de alta y dejarlo
	 * mas limpio.
	 * 
	 * @see #altaBaraja(String, float, float, int, TipoBaraja)
	 * @see #altaCristales(String, float, float, boolean, String)
	 * @see #altaVelas(float, float, float, float, int, AromaVela)
	 * @see #altaCliente(String, LocalDate, LocalTime, Signo, String, String,
	 *      boolean, TipoUsuario)
	 * @see Pintar#errorOrdenarPorCodigo()
	 * 
	 */
	private void ordenaListaPorCodigo() {
		try {
			Collections.sort(productos);
			Collections.sort(users);
			Collections.sort(pedidos);
		} catch (NullPointerException e) {
			Pintar.errorOrdenarPorCodigo();
		}
	}

	/**
	 * Recibe un numero 'n'. Recorre el arraylist de productos comparando el tipo de
	 * producto con el valor de la posicion 'n' del enum TipoProducto. De esta
	 * manera entra a comprobar si el producto tiene el codigo vacio, y si lo tiene
	 * le pone un codigo u otro segun el tipo de producto. Hay un contador que ira
	 * sumando +1 cada vez que pase de largo de un producto que si tenga codigo, de
	 * manera que el codigo de producto sea consecutivo al anterior.
	 * 
	 * @param n int
	 * 
	 */
	private void calcularCodigo(int n) {
		int i = 0;
		for (Producto p1 : productos) {
			if (p1.getTipo() == TipoProducto.values()[n] && TipoProducto.values()[n] == TipoProducto.BARAJA) {
				if (p1.getCodigo() == null) {
					productos.remove(p1);
					p1.setCodigo("BAR" + i);
					productos.add(p1);
					break;
				}
				i++;
			}
			if (p1.getTipo() == TipoProducto.values()[n] && TipoProducto.values()[n] == TipoProducto.CRISTAL) {
				if (p1.getCodigo() == null) {
					productos.remove(p1);
					p1.setCodigo("CRI" + i);
					productos.add(p1);
					break;
				}
				i++;
			}
			if (p1.getTipo() == TipoProducto.values()[n] && TipoProducto.values()[n] == TipoProducto.VELA) {
				if (p1.getCodigo() == null) {
					productos.remove(p1);
					p1.setCodigo("VEL" + i);
					productos.add(p1);
					break;
				}
				i++;
			}
		}
	}

	/**
	 * No recibe nada. Devuelve el nick solamente si es "0" o si cumple las
	 * condiciones (no estar repetido). Para comprobar si esta repetido recorre el
	 * arraylist de usuarios comparandolo con el resto de nicknames. Uso un booleano
	 * para saber si puedo devolver o no el nick tras la comparacion.
	 * 
	 * @return nick String
	 * 
	 * @see Pintar#nickRepetido()
	 * @see Programa#pedirDatosString()
	 * 
	 */
	private String pedirNickname() {
		String nick = "";
		boolean nickRepe = false;
		while (!nick.equals("0")) {
			nick = Programa.pedirDatosString();
			if (!nick.equals("0")) {
				for (User u : users) {
					if (nick.equals(u.getNickname())) {
						Pintar.nickRepetido();
						nickRepe = true;
						break;
					}
				}
				if (!nickRepe) {
					return nick;
				}
			}
		}
		return nick;
	}

	/*
	 * *****************************************************************************
	 * ****************************** -----ENUMS----- ******************************
	 * *****************************************************************************
	 */

	/**
	 * Devuelve un valor del Enum TipoBaraja segun el String entrante. Si no lo
	 * encuentra devuelve null.
	 * 
	 * @param a String
	 * @return TipoBaraja
	 */

	private TipoBaraja buscarTipoBaraja(String t) {
		t = t.toUpperCase();
		switch (t) {
		case "ORACULO":
			return TipoBaraja.ORACULO;
		case "TAROT":
			return TipoBaraja.TAROT;
		default:
			Pintar.valorIncorrecto();
			Pintar.opcionesTiposB();
			return null;
		}
	}

	/**
	 * Devuelve un valor del Enum AromaVela segun el String entrante. Si no lo
	 * encuentra devuelve null.
	 * 
	 * @param a String
	 * @return AromaVela
	 */
	private AromaVela buscarAroma(String a) {
		a = a.toUpperCase();
		switch (a) {
		case "NINGUNO":
			return AromaVela.NINGUNO;
		case "VAINILLA":
			return AromaVela.VAINILLA;
		case "LIMON":
			return AromaVela.LIMON;
		case "FRUTOS ROJOS":
			return AromaVela.FRUTOS_ROJOS;
		case "CANELA":
			return AromaVela.CANELA;
		default:
			Pintar.valorIncorrecto();
			Pintar.opcionesAromas();
			return null;
		}
	}

	/*
	 * *****************************************************************************
	 * ***************************** -----BUSCAR----- ******************************
	 * *****************************************************************************
	 */

	/**
	 * Pide un String y recorre el ArrayList de productos con un bucle for each
	 * comparando si el codigo de los objetos que va recorriendo coincide con la
	 * cadena de texto. Va comprobando si ha encontrado o no el producto con un
	 * boolean, de manera que si no se encuentra ningun producto con ese codigo (el
	 * boolean sigue en false), mostrara un mensaje de no se encontraron
	 * coincidencias. Si lo encuentra, el boolean se cambia a true y el bucle se
	 * rompe.
	 */
	public void buscarProducto() {
		Pintar.buscarPorCodigo();
		String codigo = Programa.pedirDatosString();
		if (codigo.equals("0")) {
			Pintar.retrocediendo();
		} else {
			boolean pExiste = false;
			for (Producto p : productos) {
				if (p.getCodigo() != null) {
					if (p.getCodigo().equalsIgnoreCase(codigo)) {
						System.out.println("|           Este es el producto que has buscado:");
						System.out.println(p);
						pExiste = true;
						break;
					}
				}
			}
			if (!pExiste) {
				Pintar.productoNoExiste();
			}
		}
	}

	/**
	 * Recibe una cadena de texto y recorre cada baraja en un bucle for each que
	 * compara el tipo de producto para no recorrer otro que no sean barajas. En
	 * cada uno recorre un bucle for dependiendo de la largura del array de cartas
	 * de esa baraja.
	 * 
	 * Compara el nombre de cada carta con el String recibido. Si encuentra una
	 * coincidencia, muestra la carta.
	 * 
	 * Si no encuentra coincidencia, muestra un mensaje de que no se ha encontrado
	 * dicha carta.
	 * 
	 * No se detendra con un break tras encontrarla puesto que podria haber mas de
	 * una con el mismo nombre (no las hay, porque solo tengo dos barajas, pero
	 * podria ser).
	 * 
	 * @see Pintar#pedirNombreCarta()
	 * @see Pintar#retrocediendo()
	 * @see Pintar#cartaNoExiste()
	 * @see Pintar#esoEsTodo()
	 * @see Programa#pedirDatosString()
	 * 
	 */
	public void buscarCarta() {
		Pintar.pedirNombreCarta();
		String nombre = Programa.pedirDatosString();
		if (nombre.equals("0")) {
			Pintar.retrocediendo();
		} else {
			boolean cExiste = false;
			for (Producto p : productos) {
				if (p.getTipo() == TipoProducto.BARAJA) { // compruebo que sean barajas antes de castear
					Baraja b = (Baraja) p; // necesario casteo, si no no puedo llamar al array de cartas
					for (int i = 0; i < b.getCartas().length; i++) {
						if (b.getCartas()[i] != null) {
							if (b.getCartas()[i].getNombre().equalsIgnoreCase(nombre)) {
								System.out.println(b.getCartas()[i]);
								cExiste = true;
							}
						}
					}
				}
			}
			if (!cExiste) {
				Pintar.cartaNoExiste();
			} else {
				Pintar.esoEsTodo();
			}
		}
	}

	/**
	 * Pide un Enum AromaVela y lo compara con el atributo de las velas existentes.
	 * Si encuentra concidencias, las va mostrando por pantalla, y si no, muestra un
	 * mensaje de que no hay coincidencias.
	 * 
	 * @see Pintar#pedirAromaVela()
	 * @see Pintar#retrocediendo()
	 * @see Pintar#noHayCoincidencias()
	 * @see Pintar#esoEsTodo()
	 * @see Programa#pedirDatosString()
	 * @see #buscarAroma(String)
	 * 
	 */
	public void buscarVelaPorAroma() {
		Pintar.pedirAromaVela();
		String a = "";
		AromaVela aroma = null;
		while (aroma == null) {
			a = Programa.pedirDatosString();
			if (a.equals("0")) {
				break;
			}
			aroma = buscarAroma(a);
		}
		if (a.equals("0")) {
			Pintar.retrocediendo();
		} else {
			boolean vExiste = false;
			for (Producto p : productos) {
				if (p.getTipo() == TipoProducto.VELA) { // compruebo que sean velas antes de castear
					Vela v = (Vela) p; // necesario el casteo, si no no puedo llamar al aroma de la vela
					if (v.getAroma() != null) {
						if (v.getAroma() == aroma) {
							System.out.println(v);
							vExiste = true;
						}
					}
				}
			}
			if (!vExiste) {
				Pintar.noHayCoincidencias();
			} else {
				Pintar.esoEsTodo();
			}
		}
	}

	/**
	 * Pide un String y lo compara con cada forma de cada cristal existente. Si
	 * encuentra coincidencias, muestra el/los cristal/es por pantalla, y si no
	 * encuentra ninguna, muestra un mensaje de que no hay coincidencias.
	 * 
	 * @see Pintar#pedirForma()
	 * @see Pintar#retrocediendo()
	 * @see Pintar#noHayCoincidencias()
	 * @see Pintar#esoEsTodo()
	 * @see Programa#pedirDatosString()
	 * 
	 */
	public void buscarCristalPorForma() {
		Pintar.pedirForma();
		String forma = Programa.pedirDatosString();
		if (forma.equals("0")) {
			Pintar.retrocediendo();
		} else {
			boolean cExiste = false;
			for (Producto p : productos) {
				if (p.getTipo() == TipoProducto.CRISTAL) { // compruebo que sean cristales antes de castear
					Cristal c = (Cristal) p; // se necesita castear el prodcuto a cristal para acceder al atributo forma
					if (c.getForma() != null) {
						if (c.getForma().equalsIgnoreCase(forma)) {
							System.out.println(c);
							cExiste = true;
						}
					}
				}
			}
			if (!cExiste) {
				Pintar.noHayCoincidencias();
			} else {
				Pintar.esoEsTodo();
			}
		}
	}

	/*
	 * *****************************************************************************
	 * ***************************** -----LISTAR----- ******************************
	 * *****************************************************************************
	 */

	/**
	 * En un bucle for que va del 1 a la largura del tipo de producto, llama al
	 * metodo escrito a continuacion para irlos mostrando. Si se creasen mas clases
	 * de productos, este metodo no cambiaria.
	 * 
	 * Empieza en el 1 porque el valor 0 del enum es "Ninguno"
	 * 
	 * @see #listarProductos(int)
	 * 
	 */
	public void listarProductos() {
		for (int i = 1; i < TipoProducto.values().length; i++) {
			listarProductos(i);
		}
	}

	/**
	 * Compara el numero que recibe como parametro con el valor del enum
	 * TipoProducto que se encuentra en la posicion de dicho numero. Lo recorro con
	 * un bucle for para saber con la posicion de la 1 que numero de producto estoy
	 * mostrando. Los productos han sido ordenados previamente con Collections.sort
	 * segun el metodo sobreescrito en las clases inferiores a Producto (compareTo)
	 * utilizando como referencia el codigo de producto.
	 * 
	 * @param n int
	 * 
	 * @see Producto#compareTo(Producto)
	 * 
	 */
	public void listarProductos(int n) {
		for (int i = 0; i < productos.size(); i++) {
			Producto p = productos.get(i);
			if (p.getTipo() != null && TipoProducto.values()[n] != null) {
				if (p.getTipo() == TipoProducto.values()[n]) {
					System.out.println("|           Producto " + (i + 1) + ":");
					System.out.println(p);
				}
			}
		}
	}

	public void listarUsers() {
		for (User u : users) {
			System.out.println(u);
		}
	}

	/*
	 * *****************************************************************************
	 * **************************** -----MODIFICAR----- ****************************
	 * *****************************************************************************
	 */

	public void modificarCarta() {

	}

	public void modificarVelasPorAroma() {

	}

	public void modificarCristalesPorForma() {

	}

	/**
	 * Pide un String, y, recorriendo todos los productos existentes, lo compara con
	 * los codigos de producto. Si encuentra una coincidencia, pide otro String y lo
	 * intercambia por el nombre anterior del producto.
	 * 
	 * Comprueba si ha encontrado el producto o no con un boolean, de manera que si
	 * no encuentra ninguna coincidencia, muestra un mensaje avisando de ello, y si
	 * la encuentra rompe el bucle.
	 * 
	 * En este metodo se hace control de excepciones NullPointerException
	 * 
	 * @see Pintar#retrocediendo()
	 * @see Pintar#nuevoNombre()
	 * @see Pintar#noHayCoincidencias()
	 * @see Pintar#nombreModificado()
	 * @see Pintar#error()
	 * @see Programa#pedirDatosString()
	 * 
	 */
	public void modificarProductoPorCodigo() {
		PintarMenus.menuModificar();
		String codigo = Programa.pedirDatosString();
		if (codigo.equals("0")) {
			Pintar.retrocediendo();
		} else {
			try {
				boolean pExiste = false;
				for (int i = 0; i < productos.size(); i++) {
					if (productos.get(i) != null && productos.get(i).getCodigo() != null) {
						Producto p = productos.get(i);
						if (p.getCodigo().equalsIgnoreCase(codigo)) {
							Pintar.nuevoNombre();
							String nombre = Programa.pedirDatosString();
							if (nombre.equals("0")) {
								Pintar.retrocediendo();
								break;
							} else {
								p.setNombre(nombre);
								Pintar.nombreModificado();
								pExiste = true;
								break;
							}
						}
					}
				}
				if (!pExiste) {
					Pintar.noHayCoincidencias();
				}
			} catch (NullPointerException e) {
				Pintar.error();
			}
		}
	}

	/*
	 * *****************************************************************************
	 * ***************************** -----BORRAR----- ******************************
	 * *****************************************************************************
	 */

	/**
	 * Pide un String y lo va comparando con el codigo de todos los productos
	 * almacenados.
	 * 
	 * Comprueba si ha encontrado el producto o no con un boolean, de manera que si
	 * no encuentra ninguna coincidencia, muestra un mensaje avisando de ello, y si
	 * la encuentra rompe el bucle.
	 * 
	 * En caso de querer borrar una baraja, no se permitira.
	 * 
	 * En este metodo se hace control de excepciones NullPointerException
	 * 
	 * @see Pintar#pedirCodigo()
	 * @see Pintar#retrocediendo()
	 * @see Pintar#noSePuedenBorrarBarajas()
	 * @see Programa#pedirDatosString()
	 * @see Pintar#borradoCorrecto()
	 * @see Pintar#noHayCoincidencias()
	 * @see Pintar#error()
	 * 
	 */
	public void borrarProductoPorCodigo() {
		PintarMenus.menuBorrar();
		String codigo = Programa.pedirDatosString();
		if (codigo.equals("0")) {
			Pintar.retrocediendo();
		} else if (codigo.equalsIgnoreCase("BAR000") || codigo.equalsIgnoreCase("BAR001")) {
			Pintar.noSePuedenBorrarBarajas(); // borrar barajas inaccesible aqui
		} else {
			try {
				boolean pExiste = false;
				Iterator<Producto> it = productos.iterator();
				while (it.hasNext()) {
					Producto p = it.next();
					if (p.getCodigo().equalsIgnoreCase(codigo)) {
						it.remove();
						pExiste = true;
					}
				}
				if (!pExiste) {
					Pintar.noHayCoincidencias();
				} else {
					Pintar.borradoCorrecto();
				}
			} catch (NullPointerException e) {
				Pintar.error();
			}
		}
	}

	/*
	 * *****************************************************************************
	 * **************************** -----FICHEROS----- *****************************
	 * *****************************************************************************
	 */

	public void guardarDatosProductos() {
		try {
			// abro archivo
			ObjectOutputStream f = new ObjectOutputStream(new FileOutputStream(new File(datosProductos)));
			// escribo el vector en el archivo
			f.writeObject(productos);
			Pintar.guardarDatos();
			f.close();
		} catch (FileNotFoundException e) {
			Pintar.errorArchivoNoEncontrado();
			Pintar.despedida();
			System.exit(0);
		} catch (IOException e) {
			Pintar.errorEntradaSalida();
			Pintar.despedida();
			System.exit(0);
		}
	}

	@SuppressWarnings("unchecked")
	public void cargarDatosProductos() {
		try {
			ObjectInputStream f = new ObjectInputStream(new FileInputStream(new File(datosProductos)));
			productos = (ArrayList<Producto>) f.readObject();
			Pintar.cargarDatos();
			f.close();
		} catch (ClassNotFoundException e) {
			Pintar.errorClaseNoEncontrada();
			Pintar.despedida();
			System.exit(0);
		} catch (FileNotFoundException e) {
			Pintar.errorArchivoNoEncontrado();
			Pintar.despedida();
			System.exit(0);
		} catch (IOException e) {
			Pintar.errorEntradaSalida();
			Pintar.despedida();
			System.exit(0);
		}
	}

	public void guardarDatosUsers() {
		try {
			ObjectOutputStream f = new ObjectOutputStream(new FileOutputStream(new File(datosUsers)));
			f.writeObject(users);
			Pintar.guardarDatos();
			f.close();
		} catch (FileNotFoundException e) {
			Pintar.errorArchivoNoEncontrado();
			Pintar.despedida();
			System.exit(0);
		} catch (IOException e) {
			Pintar.errorEntradaSalida();
			Pintar.despedida();
			System.exit(0);
		}
	}

	@SuppressWarnings("unchecked")
	public void cargarDatosUsers() {
		ObjectInputStream f;
		try {
			f = new ObjectInputStream(new FileInputStream(new File(datosUsers)));
			users = (ArrayList<User>) f.readObject();
			Pintar.cargarDatos();
			f.close();
		} catch (ClassNotFoundException e) {
			Pintar.errorClaseNoEncontrada();
			Pintar.despedida();
			System.exit(0);
		} catch (FileNotFoundException e) {
			Pintar.errorArchivoNoEncontrado();
			Pintar.despedida();
			System.exit(0);
		} catch (IOException e) {
			Pintar.errorEntradaSalida();
			Pintar.despedida();
			System.exit(0);
		}
	}

	public void guardarDatosPedidos() {
		try {
			ObjectOutputStream f = new ObjectOutputStream(new FileOutputStream(new File(datosPedidos)));
			f.writeObject(pedidos);
			Pintar.guardarDatos();
			f.close();
		} catch (FileNotFoundException e) {
			Pintar.errorArchivoNoEncontrado();
			Pintar.despedida();
			System.exit(0);
		} catch (IOException e) {
			Pintar.errorEntradaSalida();
			Pintar.despedida();
			System.exit(0);
		}
	}

	@SuppressWarnings("unchecked")
	public void cargarDatosPedidos() {
		ObjectInputStream f;
		try {
			f = new ObjectInputStream(new FileInputStream(new File(datosPedidos)));
			pedidos = (ArrayList<Pedido>) f.readObject();
			Pintar.cargarDatos();
			f.close();
		} catch (ClassNotFoundException e) {
			Pintar.errorClaseNoEncontrada();
			Pintar.despedida();
			System.exit(0);
		} catch (FileNotFoundException e) {
			Pintar.errorArchivoNoEncontrado();
			Pintar.despedida();
			System.exit(0);
		} catch (IOException e) {
			Pintar.errorEntradaSalida();
			Pintar.despedida();
			System.exit(0);
		}
	}

	public void guardarDatosAdmin() {
		if (!productos.isEmpty()) {
			guardarDatosProductos();
		} else {
			System.out.println("|                                                               |");
			System.out.println("|           No habia productos que guardar.                     |");
			System.out.println("|                                                               |");
		}
		if (!users.isEmpty()) {
			guardarDatosUsers();
		} else {
			System.out.println("|                                                               |");
			System.out.println("|           No habia usuarios que guardar.                      |");
			System.out.println("|                                                               |");
		}
	}

	public void cargarDatos() {
		cargarDatosProductos();
		cargarDatosUsers();
	}
}
