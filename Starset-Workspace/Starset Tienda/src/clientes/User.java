package clientes;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import enums.TipoUsuario;
import lectura.Signo;

public class User implements Comparable<User>, Serializable {

	private static final long serialVersionUID = 1L;

	private String codigo;
	private String nombre;
	private LocalDate fecha;
	private LocalTime hora;
	private Signo carta;
	private String nickname;
	private String passwd;
	private TipoUsuario tipoUser;

	public User(String cd, String n, LocalDate f, LocalTime h, Signo ct, String nick, String pd, TipoUsuario t) {
		codigo = cd;
		nombre = n;
		fecha = f;
		hora = h;
		carta = ct;
		nickname = nick;
		passwd = pd;
		tipoUser = t;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String c) {
		codigo = c;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String n) {
		nombre = n;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate f) {
		fecha = f;
	}

	public LocalTime getHora() {
		return hora;
	}

	public void setHora(LocalTime h) {
		hora = h;
	}

	public Signo getCarta() {
		return carta;
	}

	public void setCarta(Signo signo) {
		this.carta = signo;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public TipoUsuario getTipoUser() {
		return tipoUser;
	}

	public void setTipoUser(TipoUsuario tipoUser) {
		this.tipoUser = tipoUser;
	}

	@Override
	public String toString() {
		System.out.println("|      *                                                        |");
		System.out.println("|           Codigo: " + codigo);
		System.out.println("|           Nombre: " + nombre);
		System.out.println("|           "
				+ (fecha != null ? "Fecha de nacimiento: " + fecha : "La fecha de nacimiento esta vacia"));
		System.out.println(
				"|  *        " + (hora != null ? "Hora de nacimiento: " + hora : "La hora de nacimiento esta vacia"));
		System.out.println("|      *    Tipo de usuario: " + tipoUser.user());
		System.out.println("|           Password: " + passwd);
		return "|                                                               |";
	}

	@Override
	public int compareTo(User o) {
		return codigo.compareTo(o.codigo);
	}

}
