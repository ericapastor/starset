package clientes;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

import productos.Producto;

public class Pedido implements Comparable<Pedido>, Serializable {

	private static final long serialVersionUID = 1L;

	private String codigo;
	private LocalDate fechaVenta;
	private User cliente;
	private ArrayList<Producto> productos;

	public Pedido(String id, String f, User c) {
		codigo = id;
		fechaVenta = LocalDate.parse(f);
		cliente = c;
		productos = new ArrayList<Producto>();
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String c) {
		codigo = c;
	}

	public LocalDate getFechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(LocalDate f) {
		fechaVenta = f;
	}

	public User getCliente() {
		return cliente;
	}

	public void setCliente(User c) {
		cliente = c;
	}

	public ArrayList<Producto> getProductos() {
		return productos;
	}

	public void setProductos(ArrayList<Producto> p) {
		productos = p;
	}

	@Override
	public String toString() {
		final int maxLen = 10;
		return "|           Codigo: " + codigo + "\n|           Fecha de venta: " + fechaVenta
				+ "\n|           Cliente:\n" + cliente + "\n|           Productos: "
				+ (productos != null ? productos.subList(0, Math.min(productos.size(), maxLen)) : null) + "]";
	}

	@Override
	public int compareTo(Pedido o) {
		return codigo.compareTo(o.codigo);
	}

}
