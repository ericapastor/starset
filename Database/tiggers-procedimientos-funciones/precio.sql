use STARSET;

drop trigger if exists aft_ins_precio;
delimiter ~~
create trigger aft_ins_precio before insert on precio for each row
begin
	set new.precio_neto = calcularprecioneto(new.id, new.precio_bruto, new.descuento);
end;
~~
delimiter ;

drop function if exists calcularprecioneto;
delimiter ~~
create function calcularprecioneto (idP int, pb float, descu float)
returns int
begin
	declare iva_temporal float;
	set iva_temporal = (select first iva from producto
		join baraja on producto.id = baraja.id_prod
		join cristal on producto.id = cristal.id_prod
		join vela on producto.id = vela.id_prod
		where producto.id_precio = idP and
		(producto.id = baraja.id_prod or
		producto.id = cristal.id_prod or
		producto.id = vela.id_prod));
	return pb * (1 + (iva_temporal / 100)) * (1 - (descu / 100));
end;
~~
delimiter ;