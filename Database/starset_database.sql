drop database if exists STARSET;

drop user if exists 'effy';

create database STARSET;

create user 'effy' identified by '@123';

grant all privileges on starset.*to effy;

use starset;

create table precio(
	id int unsigned primary key auto_increment,
	tipo_descuento enum ('ninguno', 'san valentin', 'primavera', 'navidad'),
	descuento float,
	precio_bruto float,
	precio_neto float
);

create table producto(
	id int unsigned primary key auto_increment,
	id_precio int unsigned not null,
	foreign key (id_precio) references precio (id),
	codigo varchar (10),
	nombre varchar (25),
	peso float,
	iva float,
	tipo enum ('ninguno', 'baraja', 'vela', 'cristal')
);

create table carta(
	id int unsigned primary key auto_increment,
	nombre varchar (20),
	elemento varchar (15),
	interpretacion1 varchar (200),
	interpretacion2 varchar (200),
	consejo varchar (200),
	direccion tinyint (1)
);
create table baraja(
	id int unsigned primary key auto_increment,
	id_prod int unsigned not null,
	foreign key (id_prod) references producto (id),
	tipo_baraja enum ('tarot','oraculo')
);

create table cristal(
	id int unsigned primary key auto_increment,
	id_prod int unsigned not null,
	foreign key (id_prod) references producto (id),
	pulido tinyint (1),
	forma varchar (50)
);

create table vela(
	id int unsigned primary key auto_increment,
	id_prod int unsigned not null,
	foreign key (id_prod) references producto (id),
	color varchar (7),
	diametro float,
	altura float,
	unidades int,
	aroma enum ('ninguno', 'vainilla', 'limon', 'frutos rojos', 'canela')
);

create table pedido(
	id int unsigned primary key auto_increment,
	codigo varchar (10),
	fecha_venta date
);

create table cliente(
	id int unsigned primary key auto_increment,
	id_pedido int unsigned not null,
	foreign key (id_pedido) references pedido (id),
	codigo varchar (10),
	nombre varchar(50),
	fecha_nacimiento date,
	hora_nacimiento time
);

create table pedido_producto(
	id_pedido int unsigned not null,
	id_prod int unsigned not null,
	foreign key (id_pedido) references pedido (id),
	foreign key (id_prod) references producto (id),
	primary key (id_pedido, id_prod)
);

create table carta_baraja(
	id_baraja int unsigned not null,
	id_carta int unsigned not null,
	foreign key (id_baraja) references baraja (id),
	foreign key (id_carta) references carta (id),
	primary key (id_baraja, id_carta)
);